package com.dev.supercompareur.view.barcodescan

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.dev.supercompareur.R
import com.dev.supercompareur.context.GlobalClickScan
import com.dev.supercompareur.di.component
import com.dev.supercompareur.factory.CustomBarCodeViewModelFactory
import com.dev.supercompareur.model.statews.ResultOf
import com.dev.supercompareur.uicomponent.CustomBottomSheetDialogFragment
import com.dev.supercompareur.viewmodel.barcodescan.CustomBarCodeViewModel
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.CaptureManager
import com.journeyapps.barcodescanner.CompoundBarcodeView
import kotlinx.android.synthetic.main.layout_barcode_scan.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap
import kotlin.concurrent.schedule


class CustomBarCodeActivity : AppCompatActivity() {

    private lateinit var barcodeScannerView: CompoundBarcodeView
    private lateinit var capture: CaptureManager

    lateinit var barCodeViewModel: CustomBarCodeViewModel

    @Inject
    lateinit var customBarCodeViewModelFactory: CustomBarCodeViewModelFactory

    var callback = object : BarcodeCallback {
        override fun barcodeResult(result: BarcodeResult?) {
            result?.let {
                barcodeScannerView.barcodeView.stopDecoding()
                openBottomSheet()
                apiScanProduit(it.text)

            }
        }

        override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_barcode_scan)

        //inject
        component.inject(this)
        barCodeViewModel = ViewModelProvider(this, customBarCodeViewModelFactory).get(CustomBarCodeViewModel::class.java)

        barcodeScannerView = findViewById(R.id.zxing_barcode_scanner)

        capture = CaptureManager(this, barcodeScannerView)
        capture.initializeFromIntent(intent, savedInstanceState)

        //decode
        barcodeScannerView.decodeContinuous(callback)

        btn_ignore_scan.setOnClickListener {

            barcodeScannerView.barcodeView.stopDecoding()
            GlobalClickScan.fromScanActivity = true
            finish()

        }

        //back
        backButtonProduitScan.setOnClickListener{ finish() }

    }

    private fun apiScanProduit(codeScan: String) {
        GlobalClickScan.productScanCode = codeScan
        val requestBody = HashMap<String, String>()
        requestBody["code"] = codeScan
        barCodeViewModel.apiScanProduit(requestBody)
        barCodeViewModel.produit.observe(this, Observer { result ->
            if (!isDestroyed) {
                when (result) {
                    is ResultOf.Success -> {
                        Log.i("RESULT_SCAN", result.value.toString())
                        result.value.statusCode?.let {
                            if(it == 1005) {
                                GlobalClickScan.productState = GlobalClickScan.PRODUCT_STATE_ADD
                                //Toast.makeText(this@CustomBarCodeActivity, "PRODUCT ADD", Toast.LENGTH_SHORT).show()
                            }
                        }
                        result.value.id?.let {
                            if(it > 0) {
                                GlobalClickScan.productState = GlobalClickScan.PRODUCT_STATE_EDIT
                                //Toast.makeText(this@CustomBarCodeActivity, "PRODUCT EDIT", Toast.LENGTH_SHORT).show()
                            }
                        }
                        Timer().schedule(2000) {
                            GlobalClickScan.fromScanActivity = true
                            finish()
                        }

                    }
                    is ResultOf.Failure -> {
                        Log.i("RESULT_SCAN",result.message ?: "message par defaut")
                        Toast.makeText(this@CustomBarCodeActivity, "Erreur lors du Scan !", Toast.LENGTH_SHORT).show()
                        GlobalClickScan.fromScanActivity = true
                        finish()
                    }
                }

            }
        })
    }

    private fun openBottomSheet() {
        CustomBottomSheetDialogFragment().apply {
            show(supportFragmentManager, CustomBottomSheetDialogFragment.TAG)
        }
    }

    override fun onResume() {
        super.onResume()
        capture.onResume()
    }

    override fun onPause() {
        super.onPause()
        capture.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        capture.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        capture.onSaveInstanceState(outState)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        capture.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}