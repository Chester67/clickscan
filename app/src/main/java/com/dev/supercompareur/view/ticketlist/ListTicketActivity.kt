package com.dev.supercompareur.view.ticketlist

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.dev.supercompareur.R
import com.dev.supercompareur.di.component
import com.dev.supercompareur.model.Dao.TicketDAO
import com.dev.supercompareur.viewmodel.ListTicketViewModel
import kotlinx.android.synthetic.main.activity_list_ticket.*

class ListTicketActivity: AppCompatActivity() {

    private var TAG = "--->ListTicketActivity"
    private lateinit var viewModel: ListTicketViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_ticket)

        //adapter set
        val ticketAdapter = TicketRecyclerAdapter { ticket -> adapterOnClick(ticket) }
        val recyclerView: RecyclerView = findViewById(R.id.recycler_view)
        recyclerView.adapter = ticketAdapter

        //inject
        component.inject(this)
        viewModel = ViewModelProviders.of(this).get(ListTicketViewModel::class.java)
        viewModel.getTickets()
        viewModel.ticketList.observe(this, androidx.lifecycle.Observer {
                tickets -> ticketAdapter.submitList(tickets as List<TicketDAO>)
        })
        search.setOnClickListener {
            viewModel.addTicket(true, 145, "this is the last of ticket")
        }


    }

    private fun adapterOnClick(ticket: TicketDAO) {
        Log.i("ITEM_TAG", ticket.toString())
    }

}