package com.dev.supercompareur.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.dev.supercompareur.R
import com.dev.supercompareur.view.barcodescan.BarCodeScanActivity
import com.dev.supercompareur.view.booklist.BookListActivity
import com.dev.supercompareur.view.produitlist.ListProduitActivity
import com.dev.supercompareur.view.ticketlist.ListTicketActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener { startRStream() }
        barcode.setOnClickListener { startBarcodeScan() }
        library.setOnClickListener { startLibraryBook() }
        produit.setOnClickListener { startProduitList() }
    }

    private fun startRStream() {
        Log.i("SIMPLE_TAG", "srart is clicked")
        val intent = Intent(this, ListTicketActivity::class.java)
        startActivity(intent)
    }

    private fun startBarcodeScan() {
        val intent = Intent(this, BarCodeScanActivity::class.java)
        startActivity(intent)
    }

    private fun startLibraryBook() {
        val intent = Intent(this, BookListActivity::class.java)
        startActivity(intent)
    }

    private fun startProduitList() {
        val intent = Intent(this, ListProduitActivity::class.java)
        startActivity(intent)
    }
}
