package com.dev.supercompareur.view.utilisateur

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dev.supercompareur.R
import com.dev.supercompareur.view.acceuil.AcceuilActivity
import kotlinx.android.synthetic.main.layout_login.*


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        cirLoginButton.setOnClickListener { startAcceuil() }
    }

    private fun startAcceuil() {
        val intent = Intent(this, AcceuilActivity::class.java)
        startActivity(intent)
    }

    /*
    @Inject
    lateinit var login: UtilisateurApi

    fun callWsLogin(request: Map<String, String>, onSuccess: (user: Object) -> Unit, onFailure: (t: Throwable) -> Unit) {

        login.logUser(request).enqueue(object : Callback<Object> {
            override fun onResponse(call: Call<Object>, response: Response<Object>) {
                response.body()?.let { user ->
                    Log.i(TAG, user.toString())
                    onSuccess.invoke(user)
                }
            }

            override fun onFailure(call: Call<Object>, t: Throwable) {
                Log.i(TAG, t.message)
                onFailure.invoke(t)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        val requestBody = HashMap<String, String>();
        requestBody.put("login", "abc@gmail.com")
        requestBody.put("password", "123678")
        callWsLogin(requestBody,
            { user -> Log.i(TAG, user.toString())},
            { t -> Log.e("MainActivity", "onFailure: ", t) }
        )
    }*/
}