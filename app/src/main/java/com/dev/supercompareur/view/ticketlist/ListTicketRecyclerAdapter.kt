package com.dev.supercompareur.view.ticketlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dev.supercompareur.R
import com.dev.supercompareur.model.Dao.TicketDAO

class TicketRecyclerAdapter(private val onClick: (TicketDAO) -> Unit) :
    ListAdapter<TicketDAO, TicketRecyclerAdapter.FlowerViewHolder>(FlowerDiffCallback) {

    /* ViewHolder for Flower, takes in the inflated view and the onClick behavior. */
    class FlowerViewHolder(itemView: View, val onClick: (TicketDAO) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        private val ticketTextView: TextView = itemView.findViewById(R.id.ticket_text)
        //private val flowerImageView: ImageView = itemView.findViewById(R.id.flower_image)
        private var currentTicket: TicketDAO? = null

        init {
            itemView.setOnClickListener {
                /*currentTicket?.let {
                    onClick(it)
                }*/
            }
        }

        /* Bind flower name and image. */
        fun bind(flower: TicketDAO) {
            currentTicket = flower

            ticketTextView.text = flower.description
            /*if (flower.image != null) {
                flowerImageView.setImageResource(flower.image)
            } else {
                flowerImageView.setImageResource(R.drawable.rose)
            }*/
        }
    }

    /* Creates and inflates view and return FlowerViewHolder. */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlowerViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.ticket_item, parent, false)
        return FlowerViewHolder(view, onClick)
    }

    /* Gets current flower and uses it to bind view. */
    override fun onBindViewHolder(holder: FlowerViewHolder, position: Int) {
        val flower = getItem(position)
        holder.bind(flower)

    }
}

object FlowerDiffCallback : DiffUtil.ItemCallback<TicketDAO>() {
    override fun areItemsTheSame(oldItem: TicketDAO, newItem: TicketDAO): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: TicketDAO, newItem: TicketDAO): Boolean {
        return oldItem.id == newItem.id
    }
}
