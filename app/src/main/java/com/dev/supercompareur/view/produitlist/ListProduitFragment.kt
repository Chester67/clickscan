package com.dev.supercompareur.view.produitlist

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dev.supercompareur.util.card.Card
import com.dev.supercompareur.util.card.CardView

class ListProduitFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val cardView = CardView(layoutInflater, container)
        cardView.bind(Card.fromBundle(arguments!!))
        return cardView.view
    }

    companion object {

        /** Creates a Fragment for a given [Card]  */
        fun create(card: Card): ListProduitFragment {
            Log.i("CARD_OBJECT", card.toString())
            val fragment = ListProduitFragment()
            fragment.arguments = card.toBundle()
            return fragment
        }
    }
}