package com.dev.supercompareur.view.acceuil

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.dev.supercompareur.R
import com.dev.supercompareur.model.data.DataAcceuil
import kotlinx.android.synthetic.main.activity_acceuil.*

class AcceuilFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_acceuil, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //setupViews here
        Log.i("LIFECYCLE", "onViewCreated")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.i("LIFECYCLE", "onActivityCreated")
        //adapter set
        val acceuilAdapter = AcceuilRecyclerAdapter(activity!!) { item -> adapterOnClick(item) }
        val recyclerView: RecyclerView = activity!!.findViewById(R.id.acceuil_recycler_view)
        recyclerView.adapter = acceuilAdapter
        recyclerView.recycledViewPool.setMaxRecycledViews(0, 0)
        var listData :MutableList<DataAcceuil> = mutableListOf()
        listData.add(0, DataAcceuil("HEADER", 0, "title 0", "desc 0", listOf("one", "two", "three")))
        listData.add(DataAcceuil("LIST", 1, "title 1", "desc 1", null))
        listData.add(DataAcceuil("LIST",2, "title 2", "desc 2", null))
        listData.add(DataAcceuil("LIST",3, "title 3", "desc 3", null))
        listData.add(DataAcceuil("LIST",4, "title 4", "desc 4", null))
        listData.add(DataAcceuil("LIST",5, "title 5", "desc 5", null))
        listData.add(DataAcceuil("LIST",6, "title 6", "desc 6", null))
        listData.add(DataAcceuil("FOOTER",7, "title 7", "desc 7", null))
        acceuilAdapter.submitList(listData)

    }

    private fun adapterOnClick(data: DataAcceuil) {
        Log.i("ITEM_ACCEUIL_TAG", data.toString())
        //val intent = Intent(this, ListProduitActivity::class.java)
        //startActivity(intent)
    }

}