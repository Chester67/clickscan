package com.dev.supercompareur.view.acceuil

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dev.supercompareur.R
import com.dev.supercompareur.model.data.DataAcceuil

class AcceuilRecyclerAdapter (private val ctx: Context, private val onClick: (DataAcceuil) -> Unit) :
    ListAdapter<DataAcceuil, RecyclerView.ViewHolder>(AcceuilDiffCallback) {

    private val TYPE_HEADER : Int = 0
    private val TYPE_LIST : Int = 1
    private val TYPE_FOOTER : Int = 2

    override fun getItemViewType(position: Int): Int {

        var result = 99
        val item = getItem(position) as DataAcceuil
        when(item.type) {
            "HEADER" -> result = TYPE_HEADER
            "LIST" -> result = TYPE_LIST
            "FOOTER" -> result = TYPE_FOOTER
        }
        return result
    }

    class AcceuilViewHolder(itemView: View, val onClick: (DataAcceuil) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        private val titleTextView: TextView = itemView.findViewById(R.id.acceuil_title)
        private val titleImage: ImageView = itemView.findViewById(R.id.acceuil_img)
        private val buttonDetail: ViewGroup = itemView.findViewById(R.id.btn_acceuil_detail)
        private var currentData: DataAcceuil? = null

        init {
            buttonDetail.setOnClickListener {
                currentData?.let {
                    onClick(it)
                }
            }
            /*itemView.setOnClickListener {
                currentData?.let {
                    onClick(it)
                }
            }*/
        }

        fun bind(item: DataAcceuil) {
            currentData = item
            titleTextView.text = item.title
            if(item.id % 2 == 0)
                titleImage.setImageResource(R.drawable.la_voiture)
            else
                titleImage.setImageResource(R.drawable.noel_img)
        }
    }

    class AcceuilHeaderViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        //private val titleTextView: TextView = itemView.findViewById(R.id.acceuil_header_title)
        private val horizontalLinear: ViewGroup = itemView.findViewById(R.id.horizontal_acceuil_list)
        fun bind(item: DataAcceuil, ctx: Context) {
            //titleTextView.text = item.title

            item.filters?.let {
                filts ->
                for(filt in filts){
                    val layoutToInflate = LayoutInflater.from(ctx).inflate(R.layout.header_subitem_acceuil, null)
                    val params5 = LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )
                    layoutToInflate.layoutParams = params5
                    horizontalLinear.addView(layoutToInflate)
                }

            }

        }
    }

    class AcceuilFooterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(item: DataAcceuil) {

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if(viewType == TYPE_HEADER)
        {
            val header = LayoutInflater.from(parent.context).inflate(R.layout.header_item_acceuil, parent,false)
            return AcceuilHeaderViewHolder(header)
        }
        if(viewType == TYPE_FOOTER)
        {
            val footer = LayoutInflater.from(parent.context).inflate(R.layout.footer_item_acceuil, parent,false)
            return AcceuilFooterViewHolder(footer)
        }

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_acceuil, parent, false)
        return AcceuilViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)

        if (holder is AcceuilViewHolder) {
            holder.bind(item)
        } else if (holder is AcceuilFooterViewHolder) {
            holder.bind(item)
        } else {
            val castedHolderHeader: AcceuilHeaderViewHolder? = holder as? AcceuilHeaderViewHolder
            castedHolderHeader?.bind(item, ctx)
        }
    }
}

object AcceuilDiffCallback : DiffUtil.ItemCallback<DataAcceuil>() {
    override fun areItemsTheSame(oldItem: DataAcceuil, newItem: DataAcceuil): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: DataAcceuil, newItem: DataAcceuil): Boolean {
        return oldItem.id == newItem.id
    }
}

