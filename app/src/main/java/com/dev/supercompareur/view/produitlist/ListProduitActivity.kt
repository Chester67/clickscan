package com.dev.supercompareur.view.produitlist

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.dev.supercompareur.R
import com.dev.supercompareur.context.GlobalClickScan
import com.dev.supercompareur.di.component
import com.dev.supercompareur.factory.ProduitListViewModelFactory
import com.dev.supercompareur.uicomponent.CustomBottomSheetDialogFragment
import com.dev.supercompareur.util.card.Card
import com.dev.supercompareur.view.barcodescan.CustomBarCodeActivity
import com.dev.supercompareur.view.produitadd.AddProduitActivity
import com.dev.supercompareur.viewmodel.produitlist.ListProduitViewModel
import kotlinx.android.synthetic.main.activity_produit_list.*
import javax.inject.Inject



class ListProduitActivity : FragmentActivity() {

    private var TAG = "--->ListProduitActivity"

    private lateinit var listProduitViewModel: ListProduitViewModel

    private lateinit var viewPager: ViewPager2

    @Inject
    lateinit var produitListViewModelFactory: ProduitListViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_produit_list)

        viewPager = findViewById(R.id.view_pager_produit)

        component.inject(this)
        listProduitViewModel = ViewModelProvider(this, produitListViewModelFactory).get(ListProduitViewModel::class.java)
        //callAndObserveViewModel()

        add_produit.setOnClickListener { startBarcodeScan() }

        viewPager.adapter = object : FragmentStateAdapter(this) {
            override fun createFragment(position: Int): Fragment {
                return ListProduitFragment.create(Card.DECK[position])
            }

            override fun getItemCount(): Int {
                return Card.DECK.size
            }
        }
    }

    /*private fun callAndObserveViewModel() {
        listProduitViewModel.listCurrencies()
        listProduitViewModel.currencies.observe(this, Observer { result ->
            if (!isDestroyed) {
                //bookListViewModel.isLoading.value = false
                Log.i(TAG, result.toString())
                when (result) {
                    // there is no need for type-casting, the compiler already knows that
                    is ResultOf.Success -> {
                        Log.i(TAG, result.value[0].devise)
                    }
                    // here as well
                    is ResultOf.Failure -> {
                        showErrorMessage(result.message ?: "message par defaut")
                    }
                }

            }
        })
    }*/

    fun showErrorMessage(message: String) {
        val myToast = Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
        //myToast.setGravity(Gravity.LEFT,200,200)
        myToast.show()
    }

    private fun openBottomSheet() {
        CustomBottomSheetDialogFragment().apply {
            show(supportFragmentManager, CustomBottomSheetDialogFragment.TAG)
        }
    }

    private fun startBarcodeScan() {
        val intent = Intent(this, CustomBarCodeActivity::class.java)
        startActivity(intent)
    }

    private fun startProduitActivity() {
        val intent = Intent(this, AddProduitActivity::class.java)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        if(GlobalClickScan.fromScanActivity) {
            GlobalClickScan.fromScanActivity = false
            startProduitActivity()
        }
    }

}