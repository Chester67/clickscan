package com.dev.supercompareur.view.acceuil

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.dev.supercompareur.R
import com.dev.supercompareur.context.GlobalClickScan
import com.dev.supercompareur.view.barcodescan.CustomBarCodeActivity
import com.dev.supercompareur.view.produitadd.AddProduitActivity
import com.dev.supercompareur.view.profil.ProfilFragment
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_acceuil.*

class AcceuilActivity : AppCompatActivity() {

    private lateinit var acceuilFragment: AcceuilFragment
    private lateinit var profilFragment: ProfilFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_acceuil)

        //fragments
        acceuilFragment = AcceuilFragment()
        profilFragment = ProfilFragment()
        replaceFragment(acceuilFragment)

        //left menu
        menuButton.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }
        setupNavigation()

        //scan button
        scanButton.setOnClickListener {
            startBarcodeScan()
        }

        //bottom menu
        //background
        menu_acceuil.isSelected = true
        menu_acceuil.isPressed = false
        menu_magasin.isSelected = false
        menu_magasin.isPressed = false
        menu_consommation.isSelected = false
        menu_consommation.isPressed = false
        menu_famille.isSelected = false
        menu_famille.isPressed = false

        //image
        menu_magasin_img.isPressed = false
        menu_magasin_img.isSelected = false

        menu_acceuil.setOnClickListener {
            it.isSelected = true
            it.isPressed = true

            menu_magasin.isSelected = false
            menu_magasin.isPressed = false
            menu_consommation.isSelected = false
            menu_consommation.isPressed = false
            menu_famille.isSelected = false
            menu_famille.isPressed = false

            menu_acceuil_img.isPressed = true
            menu_acceuil_img.isSelected = true
            menu_magasin_img.isPressed = false
            menu_magasin_img.isSelected = false
            menu_consommation_img.isPressed = false
            menu_consommation_img.isSelected = false
        }
        menu_magasin.setOnClickListener {
            it.isSelected = true
            it.isPressed = true

            menu_acceuil.isSelected = false
            menu_acceuil.isPressed = false
            menu_consommation.isSelected = false
            menu_consommation.isPressed = false
            menu_famille.isSelected = false
            menu_famille.isPressed = false

            menu_acceuil_img.isPressed = false
            menu_acceuil_img.isSelected = false
            menu_magasin_img.isPressed = true
            menu_magasin_img.isSelected = true
            menu_consommation_img.isPressed = false
            menu_consommation_img.isSelected = false
        }
        menu_consommation.setOnClickListener {
            it.isSelected = true
            it.isPressed = true

            menu_famille.isSelected = false
            menu_famille.isPressed = false
            menu_acceuil.isSelected = false
            menu_acceuil.isPressed = false
            menu_magasin.isSelected = false
            menu_magasin.isPressed = false

            menu_acceuil_img.isPressed = false
            menu_acceuil_img.isSelected = false
            menu_magasin_img.isPressed = false
            menu_magasin_img.isSelected = false
            menu_consommation_img.isPressed = true
            menu_consommation_img.isSelected = true
        }
        /*menu_famille.setOnClickListener {
            it.isSelected = true
            it.isPressed = true

            menu_acceuil.isSelected = false
            menu_acceuil.isPressed = false
            menu_magasin.isSelected = false
            menu_magasin.isPressed = false
            menu_consommation.isSelected = false
            menu_consommation.isPressed = false

            menu_magasin_img.isPressed = false
            menu_magasin_img.isSelected = false
        }*/
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.main_frame_layout, fragment)
            .commit()
    }

    // drawer menu listener
    private fun setupNavigation() {
        //menu
        navigationView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_camera -> {
                    Toast.makeText(this, "camera", Toast.LENGTH_SHORT).show()
                    replaceFragment(acceuilFragment)
                    closeDrawer()
                }
                R.id.nav_gallery -> {
                    Toast.makeText(this, "gallery", Toast.LENGTH_SHORT).show()
                    replaceFragment(profilFragment)
                    closeDrawer()
                }
                else -> {
                    false
                }
            }
        }

        val header: View = navigationView.getHeaderView(0)
        val closeButton = header.findViewById<View>(R.id.menuCloseButton);
        closeButton.setOnClickListener{closeDrawer()}

    }

    private fun closeDrawer(): Boolean {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        return false
    }

    private fun startBarcodeScan() {
        val intent = Intent(this, CustomBarCodeActivity::class.java)
        startActivity(intent)
    }

    private fun startProduitActivity() {
        val intent = Intent(this, AddProduitActivity::class.java)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        if(GlobalClickScan.fromScanActivity) {
            GlobalClickScan.fromScanActivity = false
            startProduitActivity()
        }
    }

}