package com.dev.supercompareur.view.produitadd

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.dev.supercompareur.R
import kotlinx.android.synthetic.main.activity_produit_add.*
import java.io.File
import com.dev.supercompareur.di.component
import android.content.DialogInterface
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.*
import android.util.Log
import android.view.MotionEvent
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import com.dev.supercompareur.factory.ProduitAddViewModelFactory
import com.dev.supercompareur.model.statews.ResultOf
import com.dev.supercompareur.util.spinner.CustomSearchableSpinner
import com.dev.supercompareur.util.spinner.interfaces.OnItemSelectListener
import com.dev.supercompareur.viewmodel.produitadd.AddProduitViewModel
import com.hootsuite.nachos.NachoTextView
import com.hootsuite.nachos.chip.Chip
import com.hootsuite.nachos.terminator.ChipTerminatorHandler
import com.hootsuite.nachos.validator.ChipifyingNachoValidator
import com.hootsuite.nachos.validator.IllegalCharacterIdentifier
import javax.inject.Inject
import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import android.view.View
import com.bumptech.glide.Glide
import com.dev.supercompareur.context.GlobalClickScan
import com.dev.supercompareur.model.data.DataProduitAdd
import com.dev.supercompareur.model.entity.PlaceEntity
import com.dev.supercompareur.model.request.ProductRequest
import com.dev.supercompareur.model.response.NearbyPlaceResponse
import com.dev.supercompareur.model.response.ProduitTypeResponse
import com.dev.supercompareur.uicomponent.CustomBottomSheetDialogFragment
import com.dev.supercompareur.util.permissions.PermissionLocationUtils
import com.dev.supercompareur.view.barcodescan.CustomBarCodeActivity
import com.google.android.gms.location.*
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.FileOutputStream


class AddProduitActivity : AppCompatActivity() {

    private var TAG = "--->AddProduitActivity"
    //Our variables
    private var mImageView: ImageView? = null
    private var mUri: Uri? = null
    private lateinit var locationCallback: LocationCallback
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var locationFound: Boolean = false
    //Field verification
    private var placeSupermarche: String? = null
    private var placeAdresse: String? = null
    private var placeIdGoogle: String? = null
    private var productScanCode: String? = null
    private var productImage: Bitmap? = null
    //Our constants
    private val OPERATION_CAPTURE_PHOTO = 1
    private val OPERATION_CHOOSE_PHOTO = 2
    private val PHOTO_PERMISSION_CODE = 3
    private val LOCATION_PERMISSION_REQUEST_CODE = 999
    //dependency inject
    private lateinit var addProduitViewModel: AddProduitViewModel
    @Inject
    lateinit var produitAddViewModelFactory: ProduitAddViewModelFactory
    //spinner
    private lateinit var searchableSpinner: CustomSearchableSpinner
    //chip type
    private var TYPE_SUGGESTIONS: MutableList<ProduitTypeResponse> = mutableListOf()
    private var TYPE_SUGGESTIONS_STRING: MutableList<String> = mutableListOf()
    private lateinit var adapter: ArrayAdapter<String>
    //observale
    //private var checkCreateIdMagasin: String? = null
    //private var checkCreateIdProduit: String? = null
    private var iterableObs: MutableList<DataProduitAdd> = mutableListOf()
    private var obsMagasinProduit = PublishSubject.create<MutableList<DataProduitAdd>>()
    private lateinit var bottomSheetDialogLoading: CustomBottomSheetDialogFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_produit_add)

        initializeWidgets()
        initDataFromScan()

        //api call
        component.inject(this)
        addProduitViewModel = ViewModelProvider(this, produitAddViewModelFactory).get(AddProduitViewModel::class.java)

        //location from database
        try {
            addProduitViewModel.dbGetAllPlaces().observe(this@AddProduitActivity, Observer { result ->
                Log.i("RESULT_DB", result.toString())
                if (result != null && result.count() > 0) {
                    Log.i("DB LIST", "List contains elements")
                    val list: MutableList<NearbyPlaceResponse> = mutableListOf()
                    for (item in result) {
                        list.add(
                            NearbyPlaceResponse(
                                item.placeStatutBusiness,
                                item.placeIcon,
                                item.placeNom,
                                item.placeIdentification,
                                item.placeAdresse
                            )
                        )
                    }
                    updateListSupermarche(list)
                }

            })
        } catch (e: Exception) {
            Log.i("db_catch", e.message)
            e.printStackTrace()
        }

        //location
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                for (location in locationResult.locations) {
                    if(!locationFound) {
                        Log.i("DB LIST", "List IS NULL")
                        val coordinate = location.latitude.toString() + "," + location.longitude.toString()
                        apiGetNearByPlace(coordinate)
                        locationFound = true
                        //"48.8773255,2.2876055" ==> cordinate format
                        Log.i("LOCATION NOW", coordinate)

                    }
                }

            }
        }
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        //click
        backButtonProduitAdd.setOnClickListener{ finish() }
        cardImageProduit.setOnClickListener{ showAlertDialogButtonClicked() }
        imgPlaceLocation.setOnClickListener{ /*getLocation()*/ }
        validerButton.setOnClickListener{onValidateProduct() }

        //api call onload activity
        apiGetTypesByCategory()

        //observable init
        obsMagasinProduit.subscribe{ result ->
            //Log.i("ANDRANA_3", result.toString())
            if(result.size == 2) {
                val inputPrix = editTextPrix?.text.toString().trim()
                var idMag = "DEF"
                var idProd = "DEF"
                for (item in result) {
                    if (item.type == "magasin")
                        idMag = item.id
                    else if (item.type == "produit")
                        idProd = item.id
                }
                if(idMag !== "DEF" && idProd !== "DEF")
                    apiAddComparateur(inputPrix, idMag, idProd)
            }
        }

    }

    fun initDataFromScan() {
        GlobalClickScan.productState?.let {
            if(it === GlobalClickScan.PRODUCT_STATE_ADD) {
                titleProduitAdd.text = "Ajouter un produit"
            } else if(it === GlobalClickScan.PRODUCT_STATE_EDIT) {
                titleProduitAdd.text = "Modifier un produit"
            }
        }

        GlobalClickScan.productScanCode?.let {
            txtCodeProduit.text = it
            productScanCode = it
        }
    }

    private fun initializeWidgets() {

        //image produit
        mImageView = findViewById(R.id.mImageView)

        //spinner magasin
        searchableSpinner = CustomSearchableSpinner(this)
        searchableSpinner.windowTitle = "SEARCHABLE SPINNER"
        searchableSpinner.onItemSelectListener = object : OnItemSelectListener {
            override fun setOnItemSelectListener(position: Int, selectedString: String, selectedObject: NearbyPlaceResponse) {
                Toast.makeText(
                    this@AddProduitActivity,
                    "${searchableSpinner.selectedItem}  ${searchableSpinner.selectedItemPosition}",
                    Toast.LENGTH_SHORT
                ).show()
                selectedObject?.let {
                    produit_text_supermarche.text = it.nom
                    produit_text_adresse.text = it.adresse
                    Glide.with(this@AddProduitActivity).load(it.icon).centerCrop().into(imageSelectedPlace)
                    placeSupermarche = it.nom
                    placeAdresse = it.adresse
                    placeIdGoogle = it.identification
                }

            }
        }

        produit_text_lieu_container.setOnClickListener{
            searchableSpinner.show()
        }

        /*textInputSpinner.editText?.keyListener = null
        textInputSpinner.editText?.setOnClickListener {
            isTextInputLayoutClicked = true
            searchableSpinner.show()
        }*/

        /*editTextSpinner.keyListener = null
        editTextSpinner.setOnClickListener {
            searchableSpinner.highlightSelectedItem = false
            isTextInputLayoutClicked = false
            searchableSpinner.show()
        }*/

        //chip
        setupChipTextView(nacho_text_view)
        //chip preload
        /*val listChip = mutableListOf<String>()
        listChip.add("test1")
        listChip.add("test2")
        nacho_text_view.setText(listChip)*/

        //nom produit listener
        editTextProduit.addTextChangedListener (object: TextWatcher {

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let {
                    if(s.isNotEmpty()) {
                        nomProduit.text = "Produit: " + s
                    } else {
                        nomProduit.text = "Produit"
                    }
                }

            }

        })

        //prix produit listener
        editTextPrix.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                Log.i(TAG, "has focus")
                if(!editTextPrix.text.isNullOrBlank()) {
                    Log.i(TAG, "enter in focus")
                    if(editTextPrix.text.length > 1) {
                        Handler(Looper.getMainLooper()).postDelayed(object : Runnable {
                            override fun run() {
                                Selection.setSelection(editTextPrix.text, editTextPrix.text.length - 1)
                            }
                        }, 200)
                    }
                }
            }
        }

        editTextPrix.addTextChangedListener (object: TextWatcher {

            override fun afterTextChanged(s: Editable?) {
                s?.toString().let {
                    if(!it.isNullOrEmpty()) {
                        if (!it.endsWith("€")) {
                            editTextPrix.setText(editTextPrix.text.toString() + "€")
                            Selection.setSelection(editTextPrix.text, editTextPrix.text.length - 1)
                            //editTextPrix.text = editTextPrix.text.toString() + " €"
                        }
                        if(it.startsWith("€")){
                            editTextPrix.setText("")
                        }

                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.toString().let {
                    if(!it.isNullOrEmpty()) {
                        prixProduit.text = "Prix: " + s
                    } else {
                        prixProduit.text = "Prix"
                    }
                }

            }

        })

        //en cas de modification produit

    }

    private fun getIdBySuggestion(list: List<ProduitTypeResponse>, suggestion: String): Int {
        for (item in list) {
            if(item.nom === suggestion)
                return  item.id.toInt()
        }
        return 0
    }

    fun bitmapToFile(bitmap: Bitmap, fileNameToSave: String): File? { // File name like "image.png"
        //create a file to write bitmap data
        var file: File? = null
        return try {
            Log.i("image_try", "ici")
            //file = File(Environment.getExternalStorageDirectory().toString() + File.separator + fileNameToSave)
            file =  File(this.getExternalFilesDir(null).toString() + File.separator + fileNameToSave)
            file.createNewFile()

            //Convert bitmap to byte array
            val bos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos) // YOU can also save it in JPEG
            val bitmapdata = bos.toByteArray()

            //write the bytes in file
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
            file
        } catch (e: Exception) {
            Log.i("image_catch", e.message)
            e.printStackTrace()
            file // it will return null
        }
    }

    private fun updateListSupermarche(listSuperMarche: List<NearbyPlaceResponse>) {
        searchableSpinner.setSpinnerListItems(listSuperMarche)
        imgPlaceLocation.visibility = View.VISIBLE
        progressBarLocation.visibility = View.INVISIBLE
    }

    //API--------------------------------------
    private fun apiGetNearByPlace(coordinate: String) {
        imgPlaceLocation.visibility = View.INVISIBLE
        progressBarLocation.visibility = View.VISIBLE
        val optionBody = HashMap<String, String>()
        optionBody["location"] = coordinate
        optionBody["rankby"] = "distance"
        optionBody["type"] = "supermarket"
        optionBody["key"] = "AIzaSyDt7EAcH6t6oh88tJddCgv5XBBFh6vp3F0"
        addProduitViewModel.nearbyPlaces(optionBody)
        addProduitViewModel.places.observe(this, Observer { result ->
            if (!isDestroyed) {
                when (result) {
                    is ResultOf.Success -> {
                        //Log.i(TAG, result.value.results[0].adresse)
                        updateListSupermarche(result.value.results)
                        GlobalClickScan.placeApiData = result.value.results
                    }
                    is ResultOf.Failure -> {
                        showErrorMessage(result.message ?: "message par defaut")
                    }
                }
            }
        })
    }

    private fun apiGetTypesByCategory() {
        val requestBody = HashMap<String, String>()
        requestBody["categorie"] = "1"
        addProduitViewModel.apiGetAllTypeByCategory(requestBody)
        addProduitViewModel.types.observe(this, Observer { result ->
            if (!isDestroyed) {
                when (result) {
                    is ResultOf.Success -> {
                        //Log.i("RESULT_TYPES", result.value.toString())
                        TYPE_SUGGESTIONS = result.value.toMutableList()
                        TYPE_SUGGESTIONS_STRING = result.value.map { it.nom }.toMutableList()
                        adapter.clear()
                        adapter.addAll(TYPE_SUGGESTIONS_STRING)
                        adapter.notifyDataSetChanged()
                    }
                    is ResultOf.Failure -> {
                        showErrorMessage(result.message ?: "message par defaut")
                    }
                }

            }
        })
    }

    private fun apiUploadProduit(produit: String, quantite: String, marque: String, types: List<String>) {
        val file = bitmapToFile(productImage!!, "produit.png")
        val fileBody: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val filePart: MultipartBody.Part = MultipartBody.Part.createFormData("file", file!!.name, fileBody)
        addProduitViewModel.apiUploadProduit(filePart)
        addProduitViewModel.upload.observe(this, Observer { result ->
            if (!isDestroyed) {
                when (result) {
                    is ResultOf.Success -> {
                        Log.i("TEST-UPLOAD", result.value.toString())
                        try {
                            apiAddDistributeur(placeSupermarche!!)
                            apiAddProduct(produit, quantite, marque, types, result.value.imageName)
                            //apres creation distributeur-magasin-produit
                            //in observer pour creation comparaison
                        } catch (e: Exception) {
                            Toast.makeText(applicationContext, "EXCEPTION_VALIDATION" + e.message, Toast.LENGTH_LONG).show()
                            Log.i("EXCEPTION_VALIDATION", e.message)
                        }
                    }
                    is ResultOf.Failure -> {
                        showErrorMessage(result.message ?: "message par defaut")
                    }
                }
            }
        })
    }

    private fun apiAddDistributeur(distributeur: String) {
        try {
            val requestBody = HashMap<String, String>()
            requestBody.put("nom", distributeur)
            addProduitViewModel.apiAddDistributeur(requestBody)
            addProduitViewModel.distributeur.observe(this, Observer { result ->
                if (!isDestroyed) {
                    //Log.i(TAG, result.toString())
                    when (result) {
                        is ResultOf.Success -> {
                            //Log.i(TAG, result.value.toString())
                            if(!result.value.id.isNullOrEmpty())
                                apiAddMagasin(result.value.id)
                        }
                        is ResultOf.Failure -> {
                            showErrorMessage(result.message ?: "message par defaut")
                        }
                    }

                }
            })
        } catch (e: Exception) {
            Toast.makeText(applicationContext, "EXCEPTION_API_DISTRI" + e.message, Toast.LENGTH_LONG).show()
            Log.i("EXCEPTION_API_DISTRI", e.message)
        }
    }

    private fun apiAddMagasin(distributeurId: String) {

        try {
            val requestBody = HashMap<String, String>()
            requestBody.put("adresse", placeAdresse!!)
            requestBody.put("identification", placeIdGoogle!!)
            requestBody.put("distributeur", distributeurId)
            addProduitViewModel.apiAddMagasin(requestBody)
            addProduitViewModel.magasin.observe(this, Observer { result ->
                if (!isDestroyed) {
                    //Log.i(TAG, result.toString())
                    when (result) {
                        is ResultOf.Success -> {
                            //Log.i(TAG, result.value.toString())
                            if(!result.value.id.isNullOrEmpty()) {
                                iterableObs.add(DataProduitAdd("magasin", result.value.id))
                                obsMagasinProduit.onNext(iterableObs)
                            }
                        }
                        is ResultOf.Failure -> {
                            showErrorMessage(result.message ?: "message par defaut")
                        }
                    }
                }
            })
        } catch (e: Exception) {
            Toast.makeText(applicationContext, "EXCEPTION_API_MAGASIN" + e.message, Toast.LENGTH_LONG).show()
            Log.i("EXCEPTION_API_MAGASIN", e.message)
        }

    }

    private fun apiAddProduct(produit: String, quantite: String, marque: String, types: List<String>, image: String) {

        try {
            //test array
            val typesToSend: ArrayList<ProductRequest> = arrayListOf()
            for(typeStr in types) {
                val idType = getIdBySuggestion(TYPE_SUGGESTIONS, typeStr)
                typesToSend.add(ProductRequest(idType))
            }

            val requestBody = HashMap<String, Any>()
            requestBody.put("nom", produit)
            requestBody.put("code", productScanCode!!)
            requestBody.put("image", image)
            requestBody.put("types", Gson().toJsonTree(typesToSend))
            requestBody.put("quantite", quantite)
            requestBody.put("marque", marque)
            requestBody.put("utilisateur", "1")
            addProduitViewModel.apiAddProduit(requestBody)
            addProduitViewModel.response.observe(this, Observer { result ->
                if (!isDestroyed) {
                    //Log.i(TAG, result.toString())
                    when (result) {
                        is ResultOf.Success -> {
                            //Log.i(TAG, result.value.toString())
                            if (!result.value.id.isNullOrEmpty()){
                                iterableObs.add(DataProduitAdd("produit",result.value.id))
                                obsMagasinProduit.onNext(iterableObs)
                            }
                        }
                        is ResultOf.Failure -> {
                            showErrorMessage(result.message ?: "message par defaut")
                        }
                    }

                }
            })
        } catch (e: Exception) {
            Toast.makeText(applicationContext, "EXCEPTION_API_PRODUIT" + e.message, Toast.LENGTH_LONG).show()
            Log.i("EXCEPTION_API_PRODUIT", e.message)
        }
    }

    private fun apiAddComparateur(prix: String, idMagasin: String, idProduit: String) {
        try {
            //Log.i("COMPARATEUR_1", "FINAL PRIX :" + prix.dropLast(1))
            //Log.i("COMPARATEUR_2", "FINAL PRODUIT :" + idMagasin)
            //Log.i("COMPARATEUR_3", "FINAL MAGASIN :" + idProduit)
            //Log.i("COMPARATEUR_4", "FINAL USER :" + )
            val requestBody = HashMap<String, String>()
            requestBody.put("prix", prix.dropLast(1))
            requestBody.put("produit", idProduit)
            requestBody.put("magasin", idMagasin)
            requestBody.put("utilisateur", "1")
            addProduitViewModel.apiAddComparateur(requestBody)
            addProduitViewModel.comparateur.observe(this, Observer { result ->
                if (!isDestroyed) {
                    Log.i(TAG, result.toString())
                    when (result) {
                        is ResultOf.Success -> {
                            Log.i(TAG, result.value.toString())
                            //iterableObs.clear()
                            //obsMagasinProduit.onNext(iterableObs)
                            closeBottomSheetLoading()
                            showAlertDialogAjout()
                        }
                        is ResultOf.Failure -> {
                            showErrorMessage(result.message ?: "message par defaut")
                            closeBottomSheetLoading()
                        }
                    }

                }
            })
        } catch (e: Exception) {
            closeBottomSheetLoading()
            Toast.makeText(applicationContext, "EXCEPTION_API_COMPARAT" + e.message, Toast.LENGTH_LONG).show()
            Log.i("EXCEPTION_API_COMPARAT", "EXEPTION_comparat" + e.message)
        }
    }

    //TOAST MESSAGE & ALERT--------------------------------
    private fun showErrorMessage(message: String) {
        val myToast = Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
        myToast.show()
    }

    private fun show(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showAlertDialogAjout() {
        val afterAddButton = arrayOf("Fermer", "Ajouter un autre produit")
        val builder = AlertDialog.Builder(this, R.style.CustomAlertDialog)
        builder.setTitle("Produit ajouté avec succès")
        builder.setItems(afterAddButton, DialogInterface.OnClickListener {
                dialog, which ->
            when(which){
                0 -> finish()
                1 -> addOtherProduct()
            }
        })
        builder.show()
    }

    private fun addOtherProduct() {
        val intent = Intent(this, CustomBarCodeActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun openBottomSheetLoading() {
        bottomSheetDialogLoading = CustomBottomSheetDialogFragment().apply {
            show(supportFragmentManager, CustomBottomSheetDialogFragment.TAG)
        }
    }

    private fun closeBottomSheetLoading() {
        bottomSheetDialogLoading.dialog!!.dismiss()
    }

    //FORMULAIRE VALIDATION------------------------
    private fun onValidateProduct() {
        val inputProduit = editTextProduit?.text.toString().trim()
        val inputQuantite = editTextQuantite?.text.toString().trim()
        val inputMarque = editTextMarque?.text.toString().trim()
        val inputPrix = editTextPrix?.text.toString().trim()
        val inputTypes: List<String> = nacho_text_view.chipValues
        /*Toast.makeText(
            this@AddProduitActivity,
            "PlaceIdGoogle:${placeIdGoogle}/PlaceSupermarche:${placeSupermarche}/PlaceAdresse:${placeAdresse}/Produit:${inputProduit}/Marque:${inputMarque}/Prix:${inputPrix}/CodeBare:${productScanCode}",
            Toast.LENGTH_LONG
        ).show()*/
        /*Toast.makeText(
            this@AddProduitActivity,
            "inputTypes:${inputTypes}",
            Toast.LENGTH_LONG
        ).show()*/

        if(productImage != null && !productScanCode.isNullOrEmpty() && !placeIdGoogle.isNullOrEmpty() && !placeSupermarche.isNullOrEmpty() && !placeAdresse.isNullOrEmpty() && !inputProduit.isNullOrBlank() && !inputMarque.isNullOrBlank() && !inputPrix.isNullOrBlank() && !inputTypes.isNullOrEmpty()) {
            Log.i(TAG, "CLICK ADD")
            openBottomSheetLoading()
            apiUploadProduit(inputProduit, inputQuantite, inputMarque, inputTypes)
        } else {
            Toast.makeText(
                this@AddProduitActivity,
                "Remplissez les champs obligatoires !",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    //LOCATION-------------------------------------
    private fun setUpLocationListener() {
        // for getting the current location update after every 2 seconds with high accuracy
        val locationRequest = LocationRequest().setInterval(2000).setFastestInterval(2000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        if(ContextCompat
                .checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.myLooper()
            )
        }
    }

    private fun stopLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }

    //PHOTO-----------------------------------------
    private fun loadPhoto() {
        val checkSelfPermission = ContextCompat.checkSelfPermission(this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (checkSelfPermission != PackageManager.PERMISSION_GRANTED){
            //Requests permissions to be granted to this application at runtime
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), PHOTO_PERMISSION_CODE)
        } else{
            openGallery()
        }
    }

    private fun capturePhoto(){
        val capturedImage = File(externalCacheDir, "My_Captured_Photo.jpg")
        if(capturedImage.exists()) {
            capturedImage.delete()
        }
        capturedImage.createNewFile()
        mUri = if(Build.VERSION.SDK_INT >= 24){
            FileProvider.getUriForFile(this, "info.camposha.kimagepicker.fileprovider",
                capturedImage)
        } else {
            Uri.fromFile(capturedImage)
        }

        val intent = Intent("android.media.action.IMAGE_CAPTURE")
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mUri)
        startActivityForResult(intent, OPERATION_CAPTURE_PHOTO)
    }

    private fun openGallery(){
        val intent = Intent("android.intent.action.GET_CONTENT")
        intent.type = "image/*"
        startActivityForResult(intent, OPERATION_CHOOSE_PHOTO)
    }

    private fun renderImage(imagePath: String?){
        if (imagePath != null) {
            val bitmap = BitmapFactory.decodeFile(imagePath)
            productImage = bitmap
            //mImageView?.setImageBitmap(bitmap)
            Glide.with(this@AddProduitActivity).load(imagePath).centerCrop().into(mImageView!!)
        }
        else {
            show("ImagePath is null")
        }
    }

    private fun getImagePath(uri: Uri, selection: String?): String {
        var path: String? = null
        val cursor = contentResolver.query(uri, null, selection, null, null )
        if (cursor != null){
            if (cursor.moveToFirst()) {
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA))
            }
            cursor.close()
        }
        return path!!
    }

    @TargetApi(19)
    private fun handleImageOnKitkat(data: Intent?) {
        var imagePath: String? = null
        val uri = data!!.data
        //DocumentsContract defines the contract between a documents provider and the platform.
        if (DocumentsContract.isDocumentUri(this, uri)){
            val docId = DocumentsContract.getDocumentId(uri)
            if ("com.android.providers.media.documents" == uri?.authority){
                val id = docId.split(":")[1]
                val selsetion = MediaStore.Images.Media._ID + "=" + id
                imagePath = getImagePath(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    selsetion)
            }
            else if ("com.android.providers.downloads.documents" == uri?.authority){
                val contentUri = ContentUris.withAppendedId(Uri.parse(
                    "content://downloads/public_downloads"), java.lang.Long.valueOf(docId))
                imagePath = getImagePath(contentUri, null)
            }
        }
        else if ("content".equals(uri?.scheme, ignoreCase = true)){
            imagePath = uri?.let { getImagePath(it, null) }
        }
        else if ("file".equals(uri?.scheme, ignoreCase = true)){
            imagePath = uri?.path
        }
        renderImage(imagePath)
    }

    fun showAlertDialogButtonClicked() {
        val hobbies = arrayOf("Prendre une photo", "Charger une photo")
        val builder = AlertDialog.Builder(this, R.style.CustomAlertDialog)
        builder.setTitle("Choisissez une action")
        builder.setItems(hobbies, DialogInterface.OnClickListener {
                dialog, which ->
            when(which){
                0 -> capturePhoto()
                1 -> loadPhoto()
            }
        })
        builder.show()
    }

    //COMMON LOCATION AND PHOTO---------------------------------------------------
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>
                                            , grantedResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantedResults)
        when(requestCode){
            PHOTO_PERMISSION_CODE ->
                if (grantedResults.isNotEmpty() && grantedResults.get(0) ==
                    PackageManager.PERMISSION_GRANTED){
                    openGallery()
                }else {
                    show("Photo gallery Permission Denied")
                }
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantedResults.isNotEmpty() && grantedResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionLocationUtils.isLocationEnabled(this) -> {
                            setUpLocationListener()
                        }
                        else -> {
                            PermissionLocationUtils.showGPSNotEnabledDialog(this)
                        }
                    }
                } else {
                    Toast.makeText(
                        this,
                        getString(R.string.location_permission_not_granted),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            OPERATION_CAPTURE_PHOTO ->
                if (resultCode == Activity.RESULT_OK) {
                    mUri?.let {
                        val bitmap = BitmapFactory.decodeStream(
                            getContentResolver().openInputStream(mUri!!)
                        )
                        productImage = bitmap
                        //mImageView!!.setImageBitmap(bitmap)
                        Glide.with(this@AddProduitActivity).load(mUri).centerCrop().into(mImageView!!)
                    }
                }
            OPERATION_CHOOSE_PHOTO ->
                if (resultCode == Activity.RESULT_OK) {
                    if (Build.VERSION.SDK_INT >= 19) {
                        handleImageOnKitkat(data)
                    }
                }
        }
    }

    //CHIP TYPE--------------------------------------------------
    private fun setupChipTextView(nachoTextView: NachoTextView){

        adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, TYPE_SUGGESTIONS_STRING)
        nachoTextView.setAdapter(adapter)
        nachoTextView.addChipTerminator('\n', ChipTerminatorHandler.BEHAVIOR_CHIPIFY_ALL)
        nachoTextView.addChipTerminator(' ', ChipTerminatorHandler.BEHAVIOR_CHIPIFY_TO_TERMINATOR)
        nachoTextView.addChipTerminator(';', ChipTerminatorHandler.BEHAVIOR_CHIPIFY_CURRENT_TOKEN)
        nachoTextView.setNachoValidator(ChipifyingNachoValidator())
        nachoTextView.enableEditChipOnTouch(false, true)
        nachoTextView.setOnChipClickListener(NachoTextView.OnChipClickListener {
            chip: Chip, motionEvent: MotionEvent ->
            //Log.i(TAG, "onChipClick: " + chip.getText())
        })
        nachoTextView.setOnChipRemoveListener {
            chip: Chip? ->  nachoTextView.setSelection(nachoTextView.getText().length)
        }
    }

    //LIFECYCLE ON START----------------------------
    override fun onStart() {
        super.onStart()

        // LOCATION PERMISSION CHECK
        when {
            PermissionLocationUtils.isAccessFineLocationGranted(this) -> {
                when {
                    PermissionLocationUtils.isLocationEnabled(this) -> {
                        setUpLocationListener()
                    }
                    else -> {
                        PermissionLocationUtils.showGPSNotEnabledDialog(this)
                    }
                }
            }
            else -> {
                PermissionLocationUtils.requestAccessFineLocationPermission(
                    this,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

}