package com.dev.supercompareur.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dev.supercompareur.repository.BookRepository
import com.dev.supercompareur.viewmodel.BookViewModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BookViewModelFactory @Inject constructor(private var bookRepository: BookRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return BookViewModel(bookRepository)  as T
    }
}