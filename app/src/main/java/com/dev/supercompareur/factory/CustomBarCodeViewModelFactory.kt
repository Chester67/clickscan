package com.dev.supercompareur.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dev.supercompareur.repository.ProduitRepository
import com.dev.supercompareur.viewmodel.barcodescan.CustomBarCodeViewModel
import javax.inject.Inject

class CustomBarCodeViewModelFactory @Inject constructor(private var produitRepository: ProduitRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CustomBarCodeViewModel(produitRepository) as T
    }
}