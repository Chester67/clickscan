package com.dev.supercompareur.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dev.supercompareur.repository.BookRepository
import com.dev.supercompareur.repository.CategoryRepository
import com.dev.supercompareur.viewmodel.BookListViewModel
import javax.inject.Inject

class BookListViewModelFactory @Inject constructor(private var categoryRepository: CategoryRepository,
                                                   private var bookRepository: BookRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return BookListViewModel(categoryRepository, bookRepository) as T
    }
}