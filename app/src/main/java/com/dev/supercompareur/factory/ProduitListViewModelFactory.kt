package com.dev.supercompareur.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dev.supercompareur.repository.ProduitRepository
import com.dev.supercompareur.viewmodel.produitlist.ListProduitViewModel
import javax.inject.Inject

class ProduitListViewModelFactory @Inject constructor(private var produitRepository: ProduitRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ListProduitViewModel(produitRepository) as T
    }
}
