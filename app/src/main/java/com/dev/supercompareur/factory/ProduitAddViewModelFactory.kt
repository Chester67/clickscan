package com.dev.supercompareur.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dev.supercompareur.repository.ComparateurRepository
import com.dev.supercompareur.repository.GoogleRepository
import com.dev.supercompareur.repository.MagasinRepository
import com.dev.supercompareur.repository.ProduitRepository
import com.dev.supercompareur.viewmodel.produitadd.AddProduitViewModel
import javax.inject.Inject

class ProduitAddViewModelFactory @Inject constructor(private var produitRepository: ProduitRepository, private var googleRepository: GoogleRepository, private var magasinRepository: MagasinRepository, private var comparateurRepository: ComparateurRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddProduitViewModel(produitRepository, googleRepository, magasinRepository, comparateurRepository) as T
    }
}