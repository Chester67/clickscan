package com.dev.supercompareur.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dev.supercompareur.model.entity.BookEntity
import com.dev.supercompareur.model.entity.CategoryEntity
import com.dev.supercompareur.repository.BookRepository
import com.dev.supercompareur.repository.CategoryRepository

class BookListViewModel(categoryRepository: CategoryRepository, private var bookRepository: BookRepository) : ViewModel() {

    var allCategories: LiveData<List<CategoryEntity>> = categoryRepository.getCategories()
    var isLoading = MutableLiveData<Boolean>()

    fun getBooksListSelectedCategory(categoryID: Long) : LiveData<List<BookEntity>> {
        return bookRepository.getBooks(categoryID)
    }

    fun deleteBook(book: BookEntity) {
        bookRepository.deleteBook(book)
    }
}