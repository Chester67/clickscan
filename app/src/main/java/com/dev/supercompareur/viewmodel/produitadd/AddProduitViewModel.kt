package com.dev.supercompareur.viewmodel.produitadd

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dev.supercompareur.model.entity.PlaceEntity
import com.dev.supercompareur.model.response.*
import com.dev.supercompareur.model.statews.ResultOf
import com.dev.supercompareur.repository.ComparateurRepository
import com.dev.supercompareur.repository.GoogleRepository
import com.dev.supercompareur.repository.MagasinRepository
import com.dev.supercompareur.repository.ProduitRepository
import okhttp3.MultipartBody

class AddProduitViewModel (private var produitRepository: ProduitRepository, private var googleRepository: GoogleRepository, private var magasinRepository: MagasinRepository, private var comparateurRepository: ComparateurRepository) : ViewModel() {

    //add produit
    private val _response = MutableLiveData<ResultOf<ProduitAddResponse>>()
    val response: LiveData<ResultOf<ProduitAddResponse>>
        get() = _response

    //add distributeur
    private val _distributeur = MutableLiveData<ResultOf<DistributeurResponse>>()
    val distributeur: LiveData<ResultOf<DistributeurResponse>>
        get() = _distributeur

    //add magasin
    private val _magasin = MutableLiveData<ResultOf<MagasinResponse>>()
    val magasin: LiveData<ResultOf<MagasinResponse>>
        get() = _magasin

    //get types by category
    private val _types = MutableLiveData<ResultOf<List<ProduitTypeResponse>>>()
    val types: LiveData<ResultOf<List<ProduitTypeResponse>>>
        get() = _types

    //places
    private val _places = MutableLiveData<ResultOf<NearbyResponse>>()
    val places: LiveData<ResultOf<NearbyResponse>>
        get() = _places

    //add comparateur
    private val _comparateur = MutableLiveData<ResultOf<ComparateurResponse>>()
    val comparateur: LiveData<ResultOf<ComparateurResponse>>
        get() = _comparateur

    //upload produit
    private val _upload = MutableLiveData<ResultOf<ProduitUploadResponse>>()
    val upload: LiveData<ResultOf<ProduitUploadResponse>>
        get() = _upload

    fun apiAddMagasin(request: Map<String, String>) {
        magasinRepository.apiAddMagasin(request,
            { resp ->
                _magasin.value = ResultOf.Success(resp)

            },
            { t ->
                _magasin.value = ResultOf.Failure(t.message, t)
                Log.e("AddProduitViewModel", "onFailure: ", t)
            }
        )
    }

    fun apiAddDistributeur(request: Map<String, String>) {
        produitRepository.apiAddDistributeur(request,
            { resp ->
                _distributeur.value = ResultOf.Success(resp)

            },
            { t ->
                _distributeur.value = ResultOf.Failure(t.message, t)
                Log.e("AddProduitViewModel", "onFailure: ", t)
            }
        )

    }

    fun apiAddProduit(request: Map<String, Any>) {
        produitRepository.apiAddProduit(request,
            { resp ->
                _response.value = ResultOf.Success(resp)

            },
            { t ->
                _response.value = ResultOf.Failure(t.message, t)
                Log.e("AddProduitViewModel", "onFailure: ", t)
            }
        )

    }

    fun apiGetAllTypeByCategory(request: Map<String, String>) {
        produitRepository.apiGetAllTypeByCategory(request,
            { resp ->
                _types.value = ResultOf.Success(resp)

            },
            { t ->
                _types.value = ResultOf.Failure(t.message, t)
                Log.e("AddProduitViewModel", "onFailure: ", t)
            })
    }

    fun nearbyPlaces(options: Map<String, String>) {
        googleRepository.nearbyPlaces(
            options,
            { resp ->
                val res = ResultOf.Success(resp)
                _places.value = res
                dbDeleteAllPlaces()
                val placeList: MutableList<PlaceEntity> = mutableListOf()
                for (result in res.value.results) {
                    placeList.add(PlaceEntity(result.statutBusiness, result.icon, result.nom, result.identification, result.adresse))
                }
                dbInsertPlaces(placeList)
            },
            { t ->
                _places.value = ResultOf.Failure(t.message, t)
                Log.e("AddProduitViewModel", "onFailure: ", t)
            }
        )
    }

    fun apiAddComparateur(request: Map<String, String>) {
        comparateurRepository.apiAddComparateur(request,
            { resp ->
                _comparateur.value = ResultOf.Success(resp)

            },
            { t ->
                _comparateur.value = ResultOf.Failure(t.message, t)
                Log.e("AddProduitViewModel", "onFailure: ", t)
            }
        )
    }

    fun apiUploadProduit(request: MultipartBody.Part) {
        produitRepository.apiUploadProduit(request,
            { resp ->
                _upload.value = ResultOf.Success(resp)

            },
            { t ->
                _upload.value = ResultOf.Failure(t.message, t)
                Log.e("UploadProduitViewModel", "onFailure: ", t)
            }
        )

    }

    fun dbDeleteAllPlaces() {
        googleRepository.deleteAllPlaces()
    }

    fun dbInsertPlaces(places : List<PlaceEntity>) {
        googleRepository.insertPlaces(places)
    }

    fun dbGetAllPlaces() : LiveData<List<PlaceEntity>> {
        return googleRepository.getAllPlaces()
    }

}