package com.dev.supercompareur.viewmodel.barcodescan

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dev.supercompareur.model.response.ProduitScanResponse
import com.dev.supercompareur.model.statews.ResultOf
import com.dev.supercompareur.repository.ProduitRepository
import javax.inject.Inject

class CustomBarCodeViewModel(private var produitRepository: ProduitRepository): ViewModel() {

    //scan produit
    private val _produit = MutableLiveData<ResultOf<ProduitScanResponse>>()
    val produit: LiveData<ResultOf<ProduitScanResponse>>
        get() = _produit

    fun apiScanProduit(request: Map<String, String>) {
        produitRepository.apiScanProduit(request,
            { resp ->
                _produit.value = ResultOf.Success(resp)

            },
            { t ->
                _produit.value = ResultOf.Failure(t.message, t)
                Log.e("AddProduitViewModel", "onFailure: ", t)
            }
        )

    }

}