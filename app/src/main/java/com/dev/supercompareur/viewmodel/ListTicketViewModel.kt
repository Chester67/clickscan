package com.dev.supercompareur.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dev.supercompareur.model.Dao.TicketDAO
import com.dev.supercompareur.service.mock.ServiceTicket

class ListTicketViewModel : ViewModel() {

    private val _ticketList = MutableLiveData<List<TicketDAO>>()

    val ticketList: LiveData<List<TicketDAO>>
        get() = _ticketList

    fun getTickets() {
        ServiceTicket.getListTicket().toList().subscribe {
            tickets -> _ticketList.value = tickets as List<TicketDAO>
        }
    }

    fun addTicket(completed: Boolean, assigendId: Int, description: String) {
        ServiceTicket.addTicket(completed, assigendId, description)
        ServiceTicket.getListTicket().toList().subscribe {
                tickets -> _ticketList.value = tickets as List<TicketDAO>
        }
    }

}