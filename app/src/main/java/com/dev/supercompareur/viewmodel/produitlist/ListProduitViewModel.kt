package com.dev.supercompareur.viewmodel.produitlist

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dev.supercompareur.model.response.CurrencyResponse
import com.dev.supercompareur.model.statews.ResultOf
import com.dev.supercompareur.repository.ProduitRepository

class ListProduitViewModel (private var produitRepository: ProduitRepository) : ViewModel() {

    private val _currencies = MutableLiveData<ResultOf<List<CurrencyResponse>>>()

    val currencies: LiveData<ResultOf<List<CurrencyResponse>>>
        get() = _currencies

    fun listCurrencies() {
        produitRepository.getCurrency(
            { currencies ->
                _currencies.value = ResultOf.Success(currencies)
                Log.i("", "")

            },
            { t ->
                _currencies.value = ResultOf.Failure(t.message, t)
                Log.e("ListProduitActivity", "onFailure: ", t)
            }
        )
    }
}