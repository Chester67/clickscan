package com.dev.supercompareur.util

class PreferenceKeys {

    companion object{

        // Shared Preference Files:
        const val APP_PREFERENCES: String = "com.codingwithmitch.openapi.APP_PREFERENCES"

        // Shared Preference Keys
        const val PREVIOUS_AUTH_USER: String = "com.codingwithmitch.openapi.PREVIOUS_AUTH_USER"


    }
}