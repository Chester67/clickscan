package com.dev.supercompareur.util.spinner.component

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev.supercompareur.R
import com.dev.supercompareur.model.response.NearbyPlaceResponse
import com.dev.supercompareur.util.spinner.interfaces.OnItemSelectListener
import kotlinx.android.synthetic.main.list_item_seachable_spinner.view.*

internal class SpinnerRecyclerAdapter(
    private val context: Context,
    private val list: List<NearbyPlaceResponse>,
    private val onItemSelectListener: OnItemSelectListener
) :
    RecyclerView.Adapter<SpinnerRecyclerAdapter.SpinnerHolder>() {

    private var spinnerListItems: List<NearbyPlaceResponse> = list
    private lateinit var selectedItem: String
    var highlightSelectedItem: Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpinnerHolder {
        return SpinnerHolder(
            LayoutInflater.from(context).inflate(
                R.layout.list_item_seachable_spinner,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return spinnerListItems.size
    }

    override fun onBindViewHolder(holder: SpinnerHolder, position: Int) {
        val nom = spinnerListItems[position].nom
        val adresse = spinnerListItems[position].adresse
        val image = spinnerListItems[position].icon
        holder.textViewSpinnerItem.text = nom
        holder.textViewSpinnerAdresse.text = adresse
        Glide.with(context).load(image).centerCrop().into(holder.imageViewSpinnerPlace)
        if (highlightSelectedItem && ::selectedItem.isInitialized) {
            val colorDrawable: ColorDrawable =
                if (nom.equals(selectedItem, true)) {
                    ColorDrawable(
                        ContextCompat.getColor(
                            context,
                            R.color.separatorColor
                        )
                    )
                } else {
                    ColorDrawable(
                        ContextCompat.getColor(
                            context,
                            android.R.color.white
                        )
                    )
                }
            holder.textViewSpinnerItem.background = colorDrawable
        }
        holder.textViewSpinnerItem.setOnClickListener {
            holder.textViewSpinnerItem.isClickable = false
            selectedItem = nom
            notifyDataSetChanged()
            onItemSelectListener.setOnItemSelectListener(
                getOriginalItemPosition(nom),
                nom,
                spinnerListItems[position]
            )
        }
    }


    class SpinnerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewSpinnerItem: TextView = itemView.textViewSpinnerItem
        val textViewSpinnerAdresse: TextView = itemView.textViewSpinnerAdresse
        val imageViewSpinnerPlace: ImageView = itemView.imageViewPlace
    }

    fun filter(query: CharSequence?) {
        val filteredNames: MutableList<NearbyPlaceResponse> = ArrayList()
        if (query.isNullOrEmpty()) {
            filterList(list)
        } else {
            for (s in list) {
                if (s.nom.contains(query, true)) {
                    filteredNames.add(s)
                }
            }
            filterList(filteredNames)
        }
    }

    private fun filterList(filteredNames: List<NearbyPlaceResponse>) {
        spinnerListItems = filteredNames
        notifyDataSetChanged()
    }

    private fun getOriginalItemPosition(selectedString: String): Int {
        for (i in list.indices) {
            if (list[i].nom == selectedString) {
                return i
            }
        }
        return 0
    }
}