package com.dev.supercompareur.util.spinner.interfaces

import com.dev.supercompareur.model.response.NearbyPlaceResponse

interface OnItemSelectListener {
    fun setOnItemSelectListener(position: Int, selectedString: String, selectedObject: NearbyPlaceResponse)
}