package com.dev.supercompareur.util.spinner.interfaces

interface OnAnimationEnd {
    fun onAnimationEndListener(isRevealed: Boolean)
}