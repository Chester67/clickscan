package com.dev.supercompareur.repository

import android.util.Log
import com.dev.supercompareur.model.response.MagasinResponse
import com.dev.supercompareur.service.retrofit.MagasinApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MagasinRepositoryImpl(private val magasinApi: MagasinApi): MagasinRepository {

    override fun apiAddMagasin(
        request: Map<String, String>,
        onSuccess: (response: MagasinResponse) -> Unit,
        onFailure: (t: Throwable) -> Unit
    ) {
        magasinApi.addMagasin(request).enqueue(object : Callback<MagasinResponse> {
            override fun onResponse(call: Call<MagasinResponse>, response: Response<MagasinResponse>) {
                Log.i("TAG_REPOSITORY", response.toString())
                response.body()?.let { response ->
                    onSuccess.invoke(response)
                }
            }

            override fun onFailure(call: Call<MagasinResponse>, t: Throwable) {
                onFailure.invoke(t)
            }
        })
    }


}