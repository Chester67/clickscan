package com.dev.supercompareur.repository

import androidx.lifecycle.LiveData
import com.dev.supercompareur.model.entity.ProduitEntity
import com.dev.supercompareur.model.response.*
import okhttp3.MultipartBody

interface ProduitRepository {

    //database
    fun getAllProduits(): LiveData<List<ProduitEntity>>
    fun insertProduit(produit: ProduitEntity)
    fun deleteProduit(produit: ProduitEntity)
    fun updateProduit(produit: ProduitEntity)

    //api
    fun getCurrency(onSuccess: (currencies: List<CurrencyResponse>) -> Unit, onFailure: (t: Throwable) -> Unit)
    fun apiAddDistributeur(request: Map<String, String>, onSuccess: (response: DistributeurResponse) -> Unit, onFailure: (t: Throwable) -> Unit)
    fun apiAddProduit(request: Map<String, Any>, onSuccess: (response: ProduitAddResponse) -> Unit, onFailure: (t: Throwable) -> Unit)
    fun apiScanProduit(request: Map<String, String>, onSuccess: (response: ProduitScanResponse) -> Unit, onFailure: (t: Throwable) -> Unit)
    fun apiGetAllTypeByCategory(request: Map<String, String>, onSuccess: (response: List<ProduitTypeResponse>) -> Unit, onFailure: (t: Throwable) -> Unit)
    fun apiUploadProduit(request: MultipartBody.Part, onSuccess: (response: ProduitUploadResponse) -> Unit, onFailure: (t: Throwable) -> Unit)
}