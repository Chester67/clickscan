package com.dev.supercompareur.repository

import androidx.lifecycle.LiveData
import com.dev.supercompareur.model.Dao.ProduitTypeDAO
import com.dev.supercompareur.model.entity.ProduitTypeEntity
import com.dev.supercompareur.persistence.AppDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProduitTypeRepository(appDatabase: AppDatabase) {

    private var produitTypeDAO: ProduitTypeDAO = appDatabase.getProduitTypeDAO()

    fun getAllProduitTypes(): LiveData<List<ProduitTypeEntity>> {
        return produitTypeDAO.getAllProduitTypes()
    }

    fun insertProduitType(type: ProduitTypeEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            produitTypeDAO.addProduitType(type)
        }
    }

    fun deleteProduitType(type: ProduitTypeEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            produitTypeDAO.deleteProduitType(type)
        }
    }

    fun updateProduitType(type: ProduitTypeEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            produitTypeDAO.updateProduitType(type)
        }
    }
}