package com.dev.supercompareur.repository

import androidx.lifecycle.LiveData
import com.dev.supercompareur.model.Dao.CategoryDAO
import com.dev.supercompareur.model.entity.CategoryEntity
import com.dev.supercompareur.persistence.AppDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CategoryRepository(libraryDatabase: AppDatabase) {

    private var categoryDAO: CategoryDAO = libraryDatabase.getCategoryDAO()

    fun getCategories(): LiveData<List<CategoryEntity>> {
        return categoryDAO.getAllCategories()
    }

    fun insertCategory(category: CategoryEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            categoryDAO.addCategory(category)
        }
    }

    fun deleteCategory(category: CategoryEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            categoryDAO.deleteCategory(category)
        }
    }

    fun updateCategory(category: CategoryEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            categoryDAO.updateCategory(category)
        }
    }
}