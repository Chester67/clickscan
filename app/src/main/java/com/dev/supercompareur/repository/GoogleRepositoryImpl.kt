package com.dev.supercompareur.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.dev.supercompareur.model.Dao.PlaceDAO
import com.dev.supercompareur.model.Dao.ProduitDAO
import com.dev.supercompareur.model.entity.PlaceEntity
import com.dev.supercompareur.model.response.NearbyResponse
import com.dev.supercompareur.persistence.AppDatabase
import com.dev.supercompareur.service.retrofit.GoogleApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GoogleRepositoryImpl(appDatabase: AppDatabase, private val googleApi: GoogleApi): GoogleRepository {

    private var placeDAO: PlaceDAO = appDatabase.getPlaceDAO()

    override fun getAllPlaces(): LiveData<List<PlaceEntity>> {
        return placeDAO.getAllPlaces()
    }

    override fun insertPlace(place: PlaceEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            placeDAO.addPlace(place)
        }
    }

    override fun insertPlaces(places: List<PlaceEntity>) {
        CoroutineScope(Dispatchers.IO).launch {
            for (place in places) {
                placeDAO.addPlace(place)
            }
        }
    }

    override fun deleteAllPlaces() {
        CoroutineScope(Dispatchers.IO).launch {
            placeDAO.deleteAllPlaces()
        }
    }

    override fun nearbyPlaces(options: Map<String, String>, onSuccess: (response: NearbyResponse) -> Unit, onFailure: (t: Throwable) -> Unit) {
        googleApi.nearbyPlaces(options).enqueue(object : Callback<NearbyResponse> {
            override fun onResponse(call: Call<NearbyResponse>, response: Response<NearbyResponse>) {
                response.body()?.let { places ->
                    onSuccess.invoke(places)
                }
            }

            override fun onFailure(call: Call<NearbyResponse>, t: Throwable) {
                onFailure.invoke(t)
            }
        })
    }

}