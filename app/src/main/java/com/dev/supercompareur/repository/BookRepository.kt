package com.dev.supercompareur.repository

import androidx.lifecycle.LiveData
import com.dev.supercompareur.model.Dao.BookDAO
import com.dev.supercompareur.model.entity.BookEntity
import com.dev.supercompareur.persistence.AppDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BookRepository(libraryDatabase: AppDatabase) {

    private var bookDAO: BookDAO = libraryDatabase.getBookDAO()

    fun getBooks(categoryID: Long): LiveData<List<BookEntity>> {
        return bookDAO.getCategoryBooks(categoryID)
    }

    fun insertBook(book: BookEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            bookDAO.addBook(book)
        }
    }

    fun deleteBook(book: BookEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            bookDAO.deleteBook(book)
        }
    }

    fun updateBook(book: BookEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            bookDAO.updateBook(book)
        }
    }
}