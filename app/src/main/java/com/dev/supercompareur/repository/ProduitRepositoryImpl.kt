package com.dev.supercompareur.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.dev.supercompareur.model.Dao.ProduitDAO
import com.dev.supercompareur.model.entity.ProduitEntity
import com.dev.supercompareur.model.response.*
import com.dev.supercompareur.persistence.AppDatabase
import com.dev.supercompareur.service.retrofit.CurrencyApi
import com.dev.supercompareur.service.retrofit.ProduitApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProduitRepositoryImpl(appDatabase: AppDatabase, private val currency: CurrencyApi, private val produit: ProduitApi): ProduitRepository {

    override fun apiUploadProduit(
        request: MultipartBody.Part,
        onSuccess: (response: ProduitUploadResponse) -> Unit,
        onFailure: (t: Throwable) -> Unit
    ) {
        produit.uploadProduit(request).enqueue(object : Callback<ProduitUploadResponse> {
            override fun onResponse(call: Call<ProduitUploadResponse>, response: Response<ProduitUploadResponse>) {
                Log.i("TAG_REPOSITORY", response.toString())
                response.body()?.let { response ->
                    onSuccess.invoke(response)
                }
            }

            override fun onFailure(call: Call<ProduitUploadResponse>, t: Throwable) {
                onFailure.invoke(t)
            }
        })
    }

    override fun apiScanProduit(
        request: Map<String, String>,
        onSuccess: (response: ProduitScanResponse) -> Unit,
        onFailure: (t: Throwable) -> Unit
    ) {
        produit.scanProduit(request).enqueue(object : Callback<ProduitScanResponse> {
            override fun onResponse(call: Call<ProduitScanResponse>, response: Response<ProduitScanResponse>) {
                Log.i("TAG_REPOSITORY", response.toString())
                response.body()?.let { response ->
                    onSuccess.invoke(response)
                }
            }

            override fun onFailure(call: Call<ProduitScanResponse>, t: Throwable) {
                onFailure.invoke(t)
            }
        })
    }

    override fun apiAddDistributeur(
        request: Map<String, String>,
        onSuccess: (response: DistributeurResponse) -> Unit,
        onFailure: (t: Throwable) -> Unit
    ) {
        produit.addDistrubuteur(request).enqueue(object : Callback<DistributeurResponse> {
            override fun onResponse(call: Call<DistributeurResponse>, response: Response<DistributeurResponse>) {
                Log.i("TAG_REPOSITORY", response.toString())
                response.body()?.let { response ->
                    onSuccess.invoke(response)
                }
            }

            override fun onFailure(call: Call<DistributeurResponse>, t: Throwable) {
                onFailure.invoke(t)
            }
        })
    }

    override fun apiGetAllTypeByCategory(
        request: Map<String, String>,
        onSuccess: (response: List<ProduitTypeResponse>) -> Unit,
        onFailure: (t: Throwable) -> Unit
    ) {
        produit.getAllTypeByCategory(request).enqueue(object : Callback<List<ProduitTypeResponse>> {
            override fun onResponse(call: Call<List<ProduitTypeResponse>>, response: Response<List<ProduitTypeResponse>>) {
                Log.i("TAG_REPOSITORY", response.toString())
                response.body()?.let { types ->
                    onSuccess.invoke(types)
                }
            }

            override fun onFailure(call: Call<List<ProduitTypeResponse>>, t: Throwable) {
                onFailure.invoke(t)
            }
        })
    }

    override fun apiAddProduit(request: Map<String, Any>, onSuccess: (response: ProduitAddResponse) -> Unit, onFailure: (t: Throwable) -> Unit) {
        produit.addProduit(request).enqueue(object : Callback<ProduitAddResponse> {
            override fun onResponse(call: Call<ProduitAddResponse>, response: Response<ProduitAddResponse>) {
                Log.i("TAG_REPOSITORY", response.toString())
                response.body()?.let { response ->
                    onSuccess.invoke(response)
                }
            }

            override fun onFailure(call: Call<ProduitAddResponse>, t: Throwable) {
                onFailure.invoke(t)
            }
        })
    }

    override fun getCurrency(onSuccess: (currencies: List<CurrencyResponse>) -> Unit, onFailure: (t: Throwable) -> Unit) {
        currency.getCurrency().enqueue(object : Callback<List<CurrencyResponse>> {
            override fun onResponse(call: Call<List<CurrencyResponse>>, response: Response<List<CurrencyResponse>>) {
                Log.i("TAG_REPOSITORY", response.toString())
                response.body()?.let { currencies ->
                    onSuccess.invoke(currencies)
                }
            }

            override fun onFailure(call: Call<List<CurrencyResponse>>, t: Throwable) {
                onFailure.invoke(t)
            }
        })
    }

    private var produitDAO: ProduitDAO = appDatabase.getProduitDAO()

    override fun getAllProduits(): LiveData<List<ProduitEntity>> {
        return produitDAO.getAllProduits()
    }

    override fun insertProduit(produit: ProduitEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            produitDAO.addProduit(produit)
        }
    }

    override fun deleteProduit(produit: ProduitEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            produitDAO.deleteProduit(produit)
        }
    }

    override fun updateProduit(produit: ProduitEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            produitDAO.updateProduit(produit)
        }
    }

}