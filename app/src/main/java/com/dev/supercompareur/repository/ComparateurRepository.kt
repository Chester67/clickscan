package com.dev.supercompareur.repository

import com.dev.supercompareur.model.response.ComparateurResponse

interface ComparateurRepository {

    //api
    fun apiAddComparateur(request: Map<String, String>, onSuccess: (response: ComparateurResponse) -> Unit, onFailure: (t: Throwable) -> Unit)

}