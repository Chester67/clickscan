package com.dev.supercompareur.repository

import com.dev.supercompareur.model.response.MagasinResponse

interface MagasinRepository {

    //api
    fun apiAddMagasin(request: Map<String, String>, onSuccess: (response: MagasinResponse) -> Unit, onFailure: (t: Throwable) -> Unit)

}