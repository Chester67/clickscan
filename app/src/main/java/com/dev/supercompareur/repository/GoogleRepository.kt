package com.dev.supercompareur.repository

import androidx.lifecycle.LiveData
import com.dev.supercompareur.model.entity.PlaceEntity
import com.dev.supercompareur.model.entity.ProduitEntity
import com.dev.supercompareur.model.response.NearbyResponse

interface GoogleRepository {

    //database
    fun getAllPlaces(): LiveData<List<PlaceEntity>>
    fun insertPlace(place: PlaceEntity)
    fun insertPlaces(places: List<PlaceEntity>)
    fun deleteAllPlaces()

    //api
    fun nearbyPlaces(options: Map<String, String>, onSuccess: (response: NearbyResponse) -> Unit, onFailure: (t: Throwable) -> Unit)

}