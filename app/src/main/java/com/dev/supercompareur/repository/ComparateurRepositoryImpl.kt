package com.dev.supercompareur.repository

import android.util.Log
import com.dev.supercompareur.model.response.ComparateurResponse
import com.dev.supercompareur.service.retrofit.ComparateurApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ComparateurRepositoryImpl(private val comparateurApi: ComparateurApi): ComparateurRepository {

    override fun apiAddComparateur(
        request: Map<String, String>,
        onSuccess: (response: ComparateurResponse) -> Unit,
        onFailure: (t: Throwable) -> Unit
    ) {
        comparateurApi.addComparateur(request).enqueue(object : Callback<ComparateurResponse> {
            override fun onResponse(call: Call<ComparateurResponse>, response: Response<ComparateurResponse>) {
                Log.i("REPOSITORY_SUCCESS", response.toString())
                response.body()?.let { response ->
                    onSuccess.invoke(response)
                }
            }

            override fun onFailure(call: Call<ComparateurResponse>, t: Throwable) {
                Log.i("REPOSITORY_FAILURE", t.message)
                onFailure.invoke(t)
            }
        })
    }


}