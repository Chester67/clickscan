package com.dev.supercompareur.model.response

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("email") val email: String,
    @SerializedName("userStatus") val statut: String
)