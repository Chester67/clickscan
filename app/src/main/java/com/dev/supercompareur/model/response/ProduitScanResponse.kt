package com.dev.supercompareur.model.response

import com.google.gson.annotations.SerializedName

data class ProduitScanResponse(
    @SerializedName("id") val id: Int?,
    @SerializedName("statusCode") val statusCode: Int?,
    @SerializedName("message") val message: String?
)