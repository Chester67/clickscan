package com.dev.supercompareur.model.Dao

data class TicketDAO (var id: Int, var completed: Boolean, var assignedId: Int, var description: String)