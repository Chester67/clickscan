package com.dev.supercompareur.model.Dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dev.supercompareur.model.entity.PlaceEntity

@Dao
interface PlaceDAO {
    @Insert
    suspend fun addPlace(place: PlaceEntity) : Long

    @Delete
    fun deletePlace(place: PlaceEntity?)

    @Query("DELETE FROM places")
    suspend fun deleteAllPlaces()

    @Query("SELECT * FROM places")
    fun getAllPlaces() : LiveData<List<PlaceEntity>>

}