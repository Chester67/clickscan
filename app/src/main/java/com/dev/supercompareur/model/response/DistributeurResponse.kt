package com.dev.supercompareur.model.response

import com.google.gson.annotations.SerializedName

data class DistributeurResponse(
    @SerializedName("id") val id: String,
    @SerializedName("nom") val nom: String
)