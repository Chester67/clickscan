package com.dev.supercompareur.model.response

import com.google.gson.annotations.SerializedName

data class CurrencyResponse(
    @SerializedName("currency") val devise: String,
    @SerializedName("price") val prix: String
)