package com.dev.supercompareur.model.response

import com.google.gson.annotations.SerializedName

data class MagasinResponse(
    @SerializedName("id") val id: String,
    @SerializedName("adresse") val adresse: String,
    @SerializedName("identification") val identification: String,
    @SerializedName("pays") val pays: String,
    @SerializedName("region") val region: String,
    @SerializedName("departement") val departement: String,
    @SerializedName("commune") val commune: String,
    @SerializedName("codepostal") val codepostal: String
)