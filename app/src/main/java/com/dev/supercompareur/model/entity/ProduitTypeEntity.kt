package com.dev.supercompareur.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "produit_type")
data class ProduitTypeEntity (
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "produit_type_id")
    var produitTypeID: Long,
    @ColumnInfo(name = "produit_type_nom")
    var produitTypeNom: String,
    @ColumnInfo(name = "produit_type_description")
    var produitTypeDesc: String,
    @ColumnInfo(name = "produit_type_code")
    var produitTypeCode: String


)
