package com.dev.supercompareur.model.Dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dev.supercompareur.model.entity.CategoryEntity

@Dao
interface CategoryDAO {
    @Insert
    suspend fun addCategory(category: CategoryEntity) : Long

    @Update
    fun updateCategory(category: CategoryEntity)

    @Delete
    fun deleteCategory(category: CategoryEntity?)

    @Query("SELECT * FROM categories")
    fun getAllCategories() : LiveData<List<CategoryEntity>>

    @Query("SELECT * FROM categories WHERE category_id == :categoryID")
    fun getCategory(categoryID: Long) : CategoryEntity?
}