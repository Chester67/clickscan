package com.dev.supercompareur.model.Dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dev.supercompareur.model.entity.BookEntity

@Dao
interface BookDAO {
    @Insert
    suspend fun addBook(book: BookEntity) : Long

    @Update
    fun updateBook(book: BookEntity)

    @Delete
    fun deleteBook(book: BookEntity?)

    @Query("SELECT * FROM books")
    fun getAllBooks() : LiveData<List<BookEntity>>

    @Query("SELECT * FROM books WHERE book_category_id == :categoryID")
    fun getCategoryBooks(categoryID: Long) : LiveData<List<BookEntity>>
}