package com.dev.supercompareur.model.entity

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "produit",
    foreignKeys = [ForeignKey(entity = ProduitTypeEntity::class, parentColumns = ["produit_type_id"], childColumns = ["produit_produit_type_id"], onDelete = ForeignKey.CASCADE)])
class ProduitEntity (): Parcelable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "produit_id")
    var produitID: Long? = null
    @ColumnInfo(name = "produit_nom")
    var produitNom: String? = null
    @ColumnInfo(name = "produit_prix")
    var produitPrix: Double? = null
    @ColumnInfo(name = "produit_code")
    var produitCode: String? = null
    @ColumnInfo(name = "produit_produit_type_id")
    var produitTypeID: Long? = null

    constructor(produitID: Long?, produitNom: String?, produitPrix: Double?, produitTypeID: Long?) : this() {
        this.produitID = produitID
        this.produitNom = produitNom
        this.produitPrix = produitPrix
        this.produitTypeID = produitTypeID
    }

    constructor(parcel: Parcel) : this() {
        produitID = parcel.readValue(Long::class.java.classLoader) as? Long
        produitNom = parcel.readString()
        produitPrix = parcel.readValue(Double::class.java.classLoader) as? Double
        produitTypeID = parcel.readValue(Long::class.java.classLoader) as? Long
    }

    // parcelable stuff
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(produitID)
        parcel.writeString(produitNom)
        parcel.writeValue(produitPrix)
        parcel.writeValue(produitTypeID)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProduitEntity> {
        override fun createFromParcel(parcel: Parcel): ProduitEntity {
            return ProduitEntity(parcel)
        }

        override fun newArray(size: Int): Array<ProduitEntity?> {
            return arrayOfNulls(size)
        }
    }

}