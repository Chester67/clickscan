package com.dev.supercompareur.model.response

import com.google.gson.annotations.SerializedName

data class ComparateurResponse(
    @SerializedName("id") val id: String
)