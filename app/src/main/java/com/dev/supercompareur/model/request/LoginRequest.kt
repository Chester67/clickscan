package com.dev.supercompareur.model.request

class LoginRequest {

    var login: String? = null
    var password: String? = null

    constructor(login: String?, password: String?) {
        this.login = login
        this.password = password
    }

}