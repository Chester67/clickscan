package com.dev.supercompareur.model.Dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dev.supercompareur.model.entity.ProduitTypeEntity

@Dao
interface ProduitTypeDAO {
    @Insert
    fun addProduitType(type: ProduitTypeEntity) : Long

    @Update
    fun updateProduitType(type: ProduitTypeEntity)

    @Delete
    fun deleteProduitType(type: ProduitTypeEntity?)

    @Query("SELECT * FROM produit_type")
    fun getAllProduitTypes() : LiveData<List<ProduitTypeEntity>>

}