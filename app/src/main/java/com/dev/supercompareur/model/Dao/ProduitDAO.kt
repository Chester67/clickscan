package com.dev.supercompareur.model.Dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.dev.supercompareur.model.entity.ProduitEntity

@Dao
interface ProduitDAO {
    @Insert
    suspend fun addProduit(produit: ProduitEntity) : Long

    @Update
    fun updateProduit(produit: ProduitEntity)

    @Delete
    suspend fun deleteProduit(produit: ProduitEntity?)

    @Query("SELECT * FROM produit")
    fun getAllProduits() : LiveData<List<ProduitEntity>>

}