package com.dev.supercompareur.model.data

data class DataAcceuil (var type: String?, var id: Int, var title: String, var description: String, var filters: List<String>?)