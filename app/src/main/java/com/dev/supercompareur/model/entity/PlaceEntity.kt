package com.dev.supercompareur.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "places")
data class PlaceEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "place_id")
    var placeID: Long,

    @ColumnInfo(name = "place_business_status")
    var placeStatutBusiness: String,

    @ColumnInfo(name = "place_icon")
    var placeIcon: String,

    @ColumnInfo(name = "place_nom")
    var placeNom: String,

    @ColumnInfo(name = "place_identification")
    var placeIdentification: String,

    @ColumnInfo(name = "place_adresse")
    var placeAdresse: String){
    constructor(placeStatutBusiness: String, placeIcon: String, placeNom: String, placeIdentification: String, placeAdresse: String) : this(0, placeStatutBusiness, placeIcon, placeNom, placeIdentification, placeAdresse)
}
