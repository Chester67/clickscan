package com.dev.supercompareur.model.response

import com.google.gson.annotations.SerializedName

data class ProduitUploadResponse(
    @SerializedName("imagePath") val imagePath: String,
    @SerializedName("imageName") val imageName: String
)