package com.dev.supercompareur.model.response
import com.google.gson.annotations.SerializedName

data class NearbyResponse(
    @SerializedName("next_page_token") val pagetoken: String,
    @SerializedName("results") val results: List<NearbyPlaceResponse>
)