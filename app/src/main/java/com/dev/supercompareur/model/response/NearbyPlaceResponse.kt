package com.dev.supercompareur.model.response

import com.google.gson.annotations.SerializedName

data class NearbyPlaceResponse (
    @SerializedName("business_status") val statutBusiness: String,
    @SerializedName("icon") val icon: String,
    @SerializedName("name") val nom: String,
    @SerializedName("place_id") val identification: String,
    @SerializedName("vicinity") val adresse: String
)