package com.dev.supercompareur.uicomponent

import android.app.Dialog
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.databinding.DataBindingUtil
import com.dev.supercompareur.R
import com.dev.supercompareur.databinding.LayoutProduitScanBinding


class CustomBottomSheetDialogFragment : BottomSheetDialogFragment()  {

    companion object {

        const val TAG = "CustomBottomSheetDialogFragment"

    }

    private lateinit var binding: LayoutProduitScanBinding
    private lateinit var loadingAnimation: AnimationDrawable
    private var isLoading = false

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = BottomSheetDialog(requireContext(), theme)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return inflater.inflate(R.layout.layout_produit_scan, container, false)
        binding = DataBindingUtil.inflate(inflater, R.layout.layout_produit_scan, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initCartLoadingAnimation()
        startLoadingAnimation()

    }

    private fun startLoadingAnimation()
    {
        isLoading = true
        binding.ivLoading.visibility = View.VISIBLE
        loadingAnimation.start()
    }
    private fun stopLoadingAnimation()
    {
        isLoading = false
        binding.ivLoading.visibility = View.INVISIBLE
        loadingAnimation.stop()
    }
    private fun initCartLoadingAnimation()
    {
        binding.ivLoading.setBackgroundResource(R.drawable.anim_loading)
        loadingAnimation = binding.ivLoading.background as AnimationDrawable
    }

}