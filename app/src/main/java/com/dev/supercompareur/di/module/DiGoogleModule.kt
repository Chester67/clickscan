package com.dev.supercompareur.di.module

import com.dev.supercompareur.persistence.AppDatabase
import com.dev.supercompareur.repository.GoogleRepository
import com.dev.supercompareur.repository.GoogleRepositoryImpl
import com.dev.supercompareur.service.retrofit.GoogleApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DiGoogleModule {

    @Singleton
    @Provides
    fun providesGoogleRepository(appDatabase: AppDatabase,  googleApi: GoogleApi): GoogleRepository {
        return GoogleRepositoryImpl(appDatabase, googleApi)
    }

}