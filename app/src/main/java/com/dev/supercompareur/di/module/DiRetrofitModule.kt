package com.dev.supercompareur.di.module

import android.content.Context
import com.dev.supercompareur.R
import com.dev.supercompareur.di.DiApp
import com.dev.supercompareur.service.retrofit.*
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import java.io.BufferedInputStream
import java.io.InputStream
import java.security.KeyStore
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager


@Module
class DiRetrofitModule {

    //dispo pour crt https://supercompareur.fr
    fun getUnsafeOkHttpClient(): OkHttpClient {

        var mCertificateFactory: CertificateFactory =
            CertificateFactory.getInstance("X.509")
        val mContext = DiApp.getContext()
        var mInputStream = mContext.resources.openRawResource(R.raw.supercompareur)
        var mCertificate: Certificate = mCertificateFactory.generateCertificate(mInputStream)
        mInputStream.close()
        val mKeyStoreType = KeyStore.getDefaultType()
        val mKeyStore = KeyStore.getInstance(mKeyStoreType)
        mKeyStore.load(null, null)
        mKeyStore.setCertificateEntry("ca", mCertificate)

        val mTmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
        val mTrustManagerFactory = TrustManagerFactory.getInstance(mTmfAlgorithm)
        mTrustManagerFactory.init(mKeyStore)

        val mTrustManagers = mTrustManagerFactory.trustManagers

        val mSslContext = SSLContext.getInstance("SSL")
        mSslContext.init(null, mTrustManagers, null)
        val mSslSocketFactory = mSslContext.socketFactory

        val builder = OkHttpClient.Builder()
        builder.sslSocketFactory(mSslSocketFactory, mTrustManagers[0] as X509TrustManager)
        builder.hostnameVerifier { _, _ -> true }
        return builder.build()
    }

    @Provides
    @Singleton
    @Named("supercompareur")
    fun providesRetrofit(): Retrofit {
        val gsonBuilder = GsonBuilder()
        val gson = gsonBuilder.create()
        return Retrofit.Builder()
            .baseUrl("http://api.supercompareur.fr/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            //.client(getUnsafeOkHttpClient())
            .build()
    }

    @Provides
    @Singleton
    @Named("google")
    fun providesRetrofitGoogle(): Retrofit {
        val gsonBuilder = GsonBuilder()
        val gson = gsonBuilder.create()
        return Retrofit.Builder()
            .baseUrl("https://maps.googleapis.com/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Provides
    @Singleton
    fun providesLogin(@Named("supercompareur") retrofit: Retrofit): UtilisateurApi {
        return retrofit.create(UtilisateurApi::class.java)
    }

    @Provides
    @Singleton
    fun providesProduit(@Named("supercompareur") retrofit: Retrofit): ProduitApi {
        return retrofit.create(ProduitApi::class.java)
    }

    @Provides
    @Singleton
    fun providesMagasin(@Named("supercompareur") retrofit: Retrofit): MagasinApi {
        return retrofit.create(MagasinApi::class.java)
    }

    @Provides
    @Singleton
    fun providesComparateur(@Named("supercompareur") retrofit: Retrofit): ComparateurApi {
        return retrofit.create(ComparateurApi::class.java)
    }

    @Provides
    @Singleton
    fun providesCurrencies(@Named("supercompareur") retrofit: Retrofit): CurrencyApi {
        return retrofit.create(CurrencyApi::class.java)
    }

    @Provides
    @Singleton
    fun providesGoogle(@Named("google") retrofit: Retrofit): GoogleApi {
        return retrofit.create(GoogleApi::class.java)
    }

}