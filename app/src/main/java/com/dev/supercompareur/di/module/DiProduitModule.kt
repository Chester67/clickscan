package com.dev.supercompareur.di.module

import com.dev.supercompareur.persistence.AppDatabase
import com.dev.supercompareur.repository.ProduitRepository
import com.dev.supercompareur.repository.ProduitRepositoryImpl
import com.dev.supercompareur.repository.ProduitTypeRepository
import com.dev.supercompareur.service.retrofit.CurrencyApi
import com.dev.supercompareur.service.retrofit.ProduitApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DiProduitModule {

    @Singleton
    @Provides
    fun providesProduitRepository(appDatabase: AppDatabase, currency: CurrencyApi, produit: ProduitApi): ProduitRepository {
        return ProduitRepositoryImpl(appDatabase, currency, produit)
    }

    @Singleton
    @Provides
    fun providesProduitTypeRepository(appDatabase: AppDatabase): ProduitTypeRepository {
        return ProduitTypeRepository(appDatabase)
    }
}