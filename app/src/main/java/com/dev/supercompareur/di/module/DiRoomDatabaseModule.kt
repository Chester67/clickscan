package com.dev.supercompareur.di.module

import android.app.Application
import android.util.Log
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.dev.supercompareur.model.entity.BookEntity
import com.dev.supercompareur.model.entity.CategoryEntity
import com.dev.supercompareur.persistence.AppDatabase
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Singleton

@Module
class DiRoomDatabaseModule(application: Application) {

    private var libraryApplication = application
    private lateinit var libraryDatabase: AppDatabase

    companion object {
        private const val EDUCATIONAL_BOOKS_CATEGORY_ID = 1L
        private const val NOVELS_CATEGORY_ID = 2L
        private const val OTHER_BOOKS_CATEGORY_ID = 3L
    }

    private val databaseCallback = object : RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            Log.d("DiRoomDatabaseModule", "onCreate")

            addSampleBooksToDatabase()

        }
    }

    private fun addSampleBooksToDatabase() {
        val category1 = CategoryEntity(EDUCATIONAL_BOOKS_CATEGORY_ID, "Education", "Description")
        val category2 = CategoryEntity(NOVELS_CATEGORY_ID, "Roman", "Description")
        val category3 = CategoryEntity(OTHER_BOOKS_CATEGORY_ID, "Autre", "Description")

        val book1 = BookEntity(1, "Programmation en java", 10.50, EDUCATIONAL_BOOKS_CATEGORY_ID)
        val book2 = BookEntity(2, "Mathematiques", 19.10, EDUCATIONAL_BOOKS_CATEGORY_ID)
        val book3 = BookEntity(3, "Les Aventures de Joe Finn", 25.30, NOVELS_CATEGORY_ID)
        val book4 = BookEntity(4, "Chien de New York", 5.30, NOVELS_CATEGORY_ID)
        val book5 = BookEntity(5, "Astrologie", 56.99, OTHER_BOOKS_CATEGORY_ID)
        val book6 = BookEntity(6, "L'arc des sorcières", 34.99, OTHER_BOOKS_CATEGORY_ID)
        val book7 = BookEntity(7, "Je peux courir ?", 99.99, NOVELS_CATEGORY_ID)
        val book8 = BookEntity(8, "Les bases de la physique", 10.50, EDUCATIONAL_BOOKS_CATEGORY_ID)

        val categoryDAO = libraryDatabase.getCategoryDAO()
        CoroutineScope(Dispatchers.IO).launch {
        categoryDAO.addCategory(category1)
        categoryDAO.addCategory(category2)
        categoryDAO.addCategory(category3)
        }

        val bookDAO = libraryDatabase.getBookDAO()
        CoroutineScope(Dispatchers.IO).launch {
            bookDAO.addBook(book1)
            bookDAO.addBook(book2)
            bookDAO.addBook(book3)
            bookDAO.addBook(book4)
            bookDAO.addBook(book5)
            bookDAO.addBook(book6)
            bookDAO.addBook(book7)
            bookDAO.addBook(book8)
        }
    }

    @Singleton
    @Provides
    fun providesRoomDatabase(): AppDatabase {
        libraryDatabase = Room.databaseBuilder(libraryApplication, AppDatabase::class.java, "app_database")
            .fallbackToDestructiveMigration()
            .addCallback(databaseCallback)
            .build()
        return libraryDatabase
    }

    @Singleton
    @Provides
    fun providesCategoryDAO(libraryDatabase: AppDatabase) = libraryDatabase.getCategoryDAO()

    @Singleton
    @Provides
    fun providesBookDAO(libraryDatabase: AppDatabase) = libraryDatabase.getBookDAO()

    @Singleton
    @Provides
    fun providesProduitDAO(appDatabase: AppDatabase) = appDatabase.getProduitDAO()

    @Singleton
    @Provides
    fun providesProduitTypeDAO(appDatabase: AppDatabase) = appDatabase.getProduitTypeDAO()
}