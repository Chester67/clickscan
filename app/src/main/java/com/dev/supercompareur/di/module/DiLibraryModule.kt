package com.dev.supercompareur.di.module

import com.dev.supercompareur.persistence.AppDatabase
import com.dev.supercompareur.repository.BookRepository
import com.dev.supercompareur.repository.CategoryRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DiLibraryModule {

    @Singleton
    @Provides
    fun providesBookRepository(libraryDatabase: AppDatabase): BookRepository {
        return BookRepository(libraryDatabase)
    }

    @Singleton
    @Provides
    fun providesCategoryRepository(libraryDatabase: AppDatabase): CategoryRepository {
        return CategoryRepository(libraryDatabase)
    }

}