package com.dev.supercompareur.di.module

import com.dev.supercompareur.viewmodel.ListTicketViewModel
import dagger.Module
import dagger.Provides

@Module
class DiTicketViewModelModule {

    @Provides
    fun providesTicketViewModel(): ListTicketViewModel {
        return ListTicketViewModel()
    }

}