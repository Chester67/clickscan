package com.dev.supercompareur.di.module

import com.dev.supercompareur.repository.MagasinRepository
import com.dev.supercompareur.repository.MagasinRepositoryImpl
import com.dev.supercompareur.service.retrofit.MagasinApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DiMagasinModule {

    @Singleton
    @Provides
    fun providesMagasinRepository(magasinApi: MagasinApi): MagasinRepository {
        return MagasinRepositoryImpl(magasinApi)
    }

}