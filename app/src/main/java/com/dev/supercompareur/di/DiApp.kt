package com.dev.supercompareur.di

import android.app.Application
import android.content.Context
import com.dev.supercompareur.di.module.DiRoomDatabaseModule
//import com.facebook.stetho.Stetho

class DiApp : Application() {

    companion object {
        lateinit var instance: DiApp
        fun getContext(): Context {
            return instance.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        //Stetho.initializeWithDefaults(this)
        instance = this
        component = DaggerDiComponent
            .builder()
            .diRoomDatabaseModule(DiRoomDatabaseModule(this))
            .build()
    }
}

lateinit var component: DiComponent
