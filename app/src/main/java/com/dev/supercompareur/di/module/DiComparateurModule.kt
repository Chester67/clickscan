package com.dev.supercompareur.di.module

import com.dev.supercompareur.repository.ComparateurRepository
import com.dev.supercompareur.repository.ComparateurRepositoryImpl
import com.dev.supercompareur.service.retrofit.ComparateurApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DiComparateurModule {

    @Singleton
    @Provides
    fun providesComparateurRepository(comparateurApi: ComparateurApi): ComparateurRepository {
        return ComparateurRepositoryImpl(comparateurApi)
    }

}