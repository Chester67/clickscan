package com.dev.supercompareur.di

import com.dev.supercompareur.di.module.*
import com.dev.supercompareur.view.barcodescan.CustomBarCodeActivity
import com.dev.supercompareur.view.bookdetail.BookActivity
import com.dev.supercompareur.view.booklist.BookListActivity
import com.dev.supercompareur.view.produitadd.AddProduitActivity
import com.dev.supercompareur.view.produitlist.ListProduitActivity
import com.dev.supercompareur.view.ticketlist.ListTicketActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        DiRetrofitModule::class,
        DiTicketViewModelModule::class,
        DiAppModule::class,
        DiRoomDatabaseModule::class,
        DiLibraryModule::class,
        DiProduitModule::class,
        DiGoogleModule::class,
        DiMagasinModule::class,
        DiComparateurModule::class
    ]
)

interface DiComponent {
    fun inject(activity: ListTicketActivity)
    fun inject(bookListActivity: BookListActivity)
    fun inject(bookActivity: BookActivity)
    fun inject(listProduitActivity: ListProduitActivity)
    fun inject(addProduitActivity: AddProduitActivity)
    fun inject(customBarCodeActivity: CustomBarCodeActivity)
}