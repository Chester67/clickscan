package com.dev.supercompareur.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dev.supercompareur.model.Dao.*
import com.dev.supercompareur.model.entity.*

@Database(entities = [CategoryEntity::class, BookEntity::class, ProduitEntity::class, ProduitTypeEntity::class, PlaceEntity::class], version = 1)
abstract class AppDatabase: RoomDatabase() {

    abstract fun getCategoryDAO(): CategoryDAO
    abstract fun getBookDAO(): BookDAO

    abstract fun getProduitDAO(): ProduitDAO
    abstract fun getProduitTypeDAO(): ProduitTypeDAO

    abstract fun getPlaceDAO(): PlaceDAO

}
