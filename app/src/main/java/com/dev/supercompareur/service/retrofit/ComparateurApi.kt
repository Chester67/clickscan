package com.dev.supercompareur.service.retrofit

import com.dev.supercompareur.model.response.ComparateurResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ComparateurApi {

    @Headers("Content-Type: application/json")
    @POST("/v1/comparateurs/checkcreate")
    fun addComparateur(@Body body: Map<String, String>): Call<ComparateurResponse>

}