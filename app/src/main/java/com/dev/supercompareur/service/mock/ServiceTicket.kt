package com.dev.supercompareur.service.mock

import com.dev.supercompareur.model.Dao.TicketDAO
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


object ServiceTicket {
    val tickets: MutableList<TicketDAO> = mutableListOf(
        TicketDAO(1, false, 110, "this is one"),
        TicketDAO(2, false, 111, "this is two"),
        TicketDAO(3, false, 112, "this is three")
    )

    fun getListTicket(): Observable<TicketDAO> {
        return Observable.fromIterable(tickets)
            .subscribeOn(Schedulers.io())
            .delay(5, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun addTicket(completed: Boolean, assignedId: Int, description: String): Observable<TicketDAO>{
        tickets.add(TicketDAO(4, completed, assignedId, description))
        return Observable.fromIterable(tickets)
    }

}