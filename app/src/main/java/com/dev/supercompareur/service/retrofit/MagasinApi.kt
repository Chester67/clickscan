package com.dev.supercompareur.service.retrofit

import com.dev.supercompareur.model.response.MagasinResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface MagasinApi {

    @Headers("Content-Type: application/json")
    @POST("/v1/magasins/checkcreate")
    fun addMagasin(@Body body: Map<String, String>): Call<MagasinResponse>

}