package com.dev.supercompareur.service.retrofit

import com.dev.supercompareur.model.response.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface ProduitApi {

    @Headers("Content-Type: application/json")
    @POST("/v1/distributeurs/checkcreate")
    fun addDistrubuteur(@Body body: Map<String, String>): Call<DistributeurResponse>

    @Headers("Content-Type: application/json")
    @POST("/v1/produits/checkcreate")
    fun addProduit(@Body body: Map<String, @JvmSuppressWildcards Any>): Call<ProduitAddResponse>

    @Headers("Content-Type: application/json")
    @POST("/v1/produits/checkscan")
    fun scanProduit(@Body body: Map<String, String>): Call<ProduitScanResponse>

    @GET("/v1/types/findByFilter")
    fun getAllTypeByCategory(@QueryMap options: Map<String, String>): Call<List<ProduitTypeResponse>>

    @Multipart
    @POST("/v1/produits/upload")
    fun uploadProduit(@Part file: MultipartBody.Part): Call<ProduitUploadResponse>

}
