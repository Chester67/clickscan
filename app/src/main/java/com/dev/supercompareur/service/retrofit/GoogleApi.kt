package com.dev.supercompareur.service.retrofit

import com.dev.supercompareur.model.response.NearbyResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface GoogleApi {

    @GET("maps/api/place/nearbysearch/json")
    fun nearbyPlaces(@QueryMap options: Map<String, String>): Call<NearbyResponse>

}