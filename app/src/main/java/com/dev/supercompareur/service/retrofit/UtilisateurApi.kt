package com.dev.supercompareur.service.retrofit

import retrofit2.Call
import retrofit2.http.*


interface UtilisateurApi {

    //@GET("users/{user}")
    //fun getUser(@Path("user") user: String): Call<User>

    @Headers("Content-Type: application/json")
    @POST("/users/login")
    fun logUser(@Body body: Map<String, String>): Call<Object>
}