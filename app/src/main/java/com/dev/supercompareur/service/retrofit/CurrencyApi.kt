package com.dev.supercompareur.service.retrofit

import com.dev.supercompareur.model.response.CurrencyResponse
import retrofit2.Call
import retrofit2.http.GET

interface CurrencyApi {

    @GET("prices?key=8f5597bfefe4d840f9d442033bb88422")
    fun getCurrency(): Call<List<CurrencyResponse>>

}