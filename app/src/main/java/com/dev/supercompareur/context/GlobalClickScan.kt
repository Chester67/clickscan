package com.dev.supercompareur.context

import com.dev.supercompareur.model.response.NearbyPlaceResponse

class GlobalClickScan {


    companion object {
        const val PRODUCT_STATE_ADD: String = "PRODUCT_STATE_ADD"
        const val PRODUCT_STATE_EDIT: String = "PRODUCT_STATE_EDIT"

        var fromScanActivity: Boolean = false
        var productScanCode: String? = null
        var productState: String? = null
        var productName: String? = null
        var productMarque: String? = null
        var productImage: String? = null
        var placeApiData: List<NearbyPlaceResponse>? = null
    }

}