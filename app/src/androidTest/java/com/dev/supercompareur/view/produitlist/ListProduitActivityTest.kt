package com.dev.supercompareur.view.produitlist

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.dev.supercompareur.R
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class ListProduitActivityTest {

    @get:Rule
    var activityRule: ActivityScenarioRule<ListProduitActivity>
            = ActivityScenarioRule(ListProduitActivity::class.java)

    @Before
    fun setup() {

    }

    @After
    fun tearDown() {

    }

    @Test
    fun verifyAjoutProduitValidation() {
        onView(withId(R.id.add_produit)).perform(click())
        Thread.sleep(2000)
        onView(withId(R.id.auto_complete_magasin)).check(matches(isDisplayed()))
        onView(withId(R.id.editTextPrix)).perform(typeText("exemple prix"), closeSoftKeyboard())
        Thread.sleep(2000)
        onView(withId(R.id.validerButton)).perform(click())
    }

}