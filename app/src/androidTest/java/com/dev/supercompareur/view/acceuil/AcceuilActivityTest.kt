package com.dev.supercompareur.view.acceuil

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.filters.LargeTest
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dev.supercompareur.R
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import com.dev.supercompareur.view.produitlist.ListProduitActivity


@RunWith(AndroidJUnit4::class)
@LargeTest
class AcceuilActivityTest {

    @get:Rule
    var activityRule: ActivityScenarioRule<AcceuilActivity>
            = ActivityScenarioRule(AcceuilActivity::class.java)

    @Before
    fun setup() {
        Intents.init()
    }

    @After
    fun tearDown() {
        Intents.release()
    }

    @Test
    fun verifyClickToListProduitActivity() {

        // click at recyclerview itemView position 2
        //onView(withId(R.id.acceuil_recycler_view))
            //.perform(RecyclerViewActions.actionOnItemAtPosition<AcceuilRecyclerAdapter.AcceuilViewHolder>(2, click()))

        // click at recyclerview child buttonDetail inside itemView position 2
        onView(withId(R.id.acceuil_recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, clickOnViewChild(R.id.btn_acceuil_detail)))

        //test if Activity is open
        intended(hasComponent(ListProduitActivity::class.java!!.name))

    }

    private fun clickOnViewChild(viewId: Int) = object : ViewAction {
        override fun getConstraints() = null
        override fun getDescription() = "Click on a child view with specified id."
        override fun perform(uiController: UiController, view: View) = click().perform(uiController, view.findViewById<View>(viewId))
    }
}