package com.dev.supercompareur.model.Dao

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.dev.supercompareur.getOrAwaitValue
import com.dev.supercompareur.model.entity.ProduitTypeEntity
import com.dev.supercompareur.persistence.AppDatabase
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class ProduitTypeDAOTest {

    //@get:Rule
    //var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var dao: ProduitTypeDAO

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(ApplicationProvider.getApplicationContext(), AppDatabase::class.java).allowMainThreadQueries().build()
        dao = database.getProduitTypeDAO()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun inserProduitType() = runBlockingTest {
        val produitType = ProduitTypeEntity(1, "catName", "catDesc", "catCode")
        dao.addProduitType(produitType)
        val allProduitType = dao.getAllProduitTypes().getOrAwaitValue()
        assertThat(allProduitType).contains(produitType)
    }

}