package com.dev.supercompareur

import com.dev.supercompareur.view.acceuil.AcceuilActivityTest
import com.dev.supercompareur.view.produitlist.ListProduitActivityTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(AcceuilActivityTest::class, ListProduitActivityTest::class)
class AppTestSuite